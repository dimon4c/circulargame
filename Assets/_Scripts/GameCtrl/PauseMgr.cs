﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMgr : MonoBehaviour
{
    public GameObject pauseHolder, pauseBtn;
    public bool tutorialFinished = false;

    public delegate void PauseEvents();

    public event PauseEvents OnPause;

    public event PauseEvents OnResume;

    public event PauseEvents OnHome;

    private static PauseMgr instanse;
    //    public bool restart = false;

    public static bool IsReady()
    {
        return (null != instanse);
    }

    public static PauseMgr GetInstanse()
    {

        return instanse;
    }

    void Awake()
    {
        if (instanse == null)
        {
            instanse = this;
        }
    }

    public void StartPause()
    {
        pauseBtn.SetActive(false);
        pauseHolder.SetActive(true);
        PowerUpInfo.onPause = true;
        if (OnPause != null)
        {
            OnPause();
        }
    }

    public void  Resume()
    {
        PowerUpInfo.onPause = false;
        pauseBtn.SetActive(true);
        pauseHolder.SetActive(false);
        if (OnResume != null)
        {
            OnResume();
        }
    }

    public void Home()
    {
        pauseHolder.SetActive(false);

        PowerUpInfo.onPause = false;
        if (PlayerPrefs.GetInt("Tutorial", 0) == 0 || tutorialFinished)
        {
            tutorialFinished = false;
            PlayerPrefs.SetInt("Tutorial", 1);
            pauseHolder.SetActive(false);

            PowerUpInfo.onPause = false;
//            HomeCtrl.GetInstanse().AddMoneyRolling(50);

        }
        else
        {
            if (!HomeCtrl.GetInstanse().gameOverHolder.activeSelf)
                Admob.Instance.ShowInterstitial();
            HomeCtrl.GetInstanse().DeleteSave();
//            if (!PowerUpInfo.gameOver)
//                HomeCtrl.GetInstanse().CallCreateSave();
        }

        if (OnHome != null)
        {
            OnHome();
        }
    }


    public void HomeFromPause()
    {
        Home();
    }

    public void HomeFromTutorial()
    {
        tutorialFinished = true;
        PlayerPrefs.SetInt("Tutorial", 1);
        pauseHolder.SetActive(false);

        PowerUpInfo.onPause = false;

        PlayAfterTutor();
    }

    public void Restart()
    {
        PowerUpInfo.onPause = true;
        pauseHolder.SetActive(false);
        if (PlayerPrefs.GetInt("Tutorial", 0) == 0 || tutorialFinished)
        {
            tutorialFinished = false;
            PlayerPrefs.SetInt("Tutorial", 1);
            pauseHolder.SetActive(false);

            PowerUpInfo.onPause = false;
            HomeCtrl.GetInstanse().TutorialHomeFinish();


            if (OnHome != null)
            {
                OnHome();
            }

            //            return;
        }
        else
        {
            
            HomeCtrl.GetInstanse().DeleteSave();
            if (OnHome != null)
            {
                OnHome();
            }
            HomeCtrl.GetInstanse().ShowPowerUp();
           
        }
    }

    public void PlayAfterTutor()
    {
        PowerUpInfo.onPause = true;
        pauseHolder.SetActive(false);
        if (OnHome != null)
        {
            OnHome();
        }
        HomeCtrl.GetInstanse().PlayGame();
    }

    public void RestartGameOver()
    {
        PowerUpInfo.onPause = true;
        pauseHolder.SetActive(false);
        PowerUpInfo.onPause = false;
        if (PlayerPrefs.GetInt("Tutorial", 0) == 0 || tutorialFinished)
        {
            tutorialFinished = false;
            PlayerPrefs.SetInt("Tutorial", 1);
            pauseHolder.SetActive(false);

            PowerUpInfo.onPause = false;
            HomeCtrl.GetInstanse().TutorialHomeFinish();


            if (OnHome != null)
            {
                OnHome();
            }

            //            return;
        }
        else
        {
            HomeCtrl.GetInstanse().DeleteSave();
            if (OnHome != null)
            {
                OnHome();
            }
            HomeCtrl.GetInstanse().ShowPowerUp();
        }
//        HomeCtrl.GetInstanse().UIGameOverRestart();

    }

}
