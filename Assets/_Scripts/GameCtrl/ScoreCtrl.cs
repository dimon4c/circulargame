﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCtrl : MonoBehaviour
{
    public Text currentScore, hiScore;
    // Use this for initialization

    private static ScoreCtrl instanse;

    public static ScoreCtrl GetInstanse()
    {

        return instanse;
    }

    void Awake()
    {
        if (instanse == null)
        {
            instanse = this;
        }
    }



    public void UpdateScore()
    {
        currentScore.text = PowerUpInfo.playerCurrentScore.ToString();
    }

    public void UpdateHiScore()
    {
        hiScore.text = PowerUpInfo.playerHiScore.ToString();
    }
	
}
