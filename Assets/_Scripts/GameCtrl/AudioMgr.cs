﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioMgr : MonoBehaviour
{

    private static AudioMgr instanse;

    public static AudioMgr GetInstanse()
    {

        return instanse;
    }

    public List<AudioClip> audioClips;
    AudioSource audioS;

	//added
	int IsMuted = 0;

    void Awake()
    {
        if (instanse == null)
        {
            instanse = this;
        }

    }

    void Start()
    {
        audioS = GetComponents<AudioSource>()[0];

		//added
		IsMuted = PlayerPrefs.GetInt ("muted", 0);
    }

    public void PlaySound(int audioIndex)
    {
		//added
		if (IsMuted == 1)
			return;


        if (audioIndex < audioClips.Count)
            audioS.Stop();
        audioS.clip = audioClips[audioIndex];
        audioS.Play();
    }

    public void MuteAudio()
    {
		//added
		if (IsMuted == 0) {
			IsMuted = 1;
		} else {
			IsMuted = 0;
		}

		HomeCtrl.GetInstanse().mutedBtn.SetActive (IsMuted != 0);
		HomeCtrl.GetInstanse().mutedBtnPause.SetActive (IsMuted != 0);

		HomeCtrl.GetInstanse().unmutedBtn.SetActive (IsMuted == 0);
		HomeCtrl.GetInstanse().unmutedBtnPause.SetActive (IsMuted == 0);

		PlayerPrefs.SetInt ("muted", IsMuted);
    }
}
