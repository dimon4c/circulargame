﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security.Cryptography.X509Certificates;
using System.Runtime.ConstrainedExecution;

//using UnityEditor;
using System;

public class BlockGen : MonoBehaviour
{
    //    public GameObject blockHolder;
    public List<GameObject> blockTypes;
    public GameObject singleCell;
    public int singleCellsToSapwn = 0;
    [Header("MaxBlocks")]
    [Tooltip("Represents amount of blocks to create")]
    public const int maxBlocks = 3;

    public int blockOn = 0;

    const float step = 1f;
    float blockWidth = 1f;
    //TODO: blockWidth should be calculated instead of this aprox value
    float localStartX;
    bool blockUndo = true;

    public delegate void RemoveBlocks();

    public event RemoveBlocks OnRemoveBlocks;

    public int BlockOn
    {
        set
        {
            if (value > maxBlocks)
            {
                
                blockOn = maxBlocks;
            }
            else
                blockOn = value;
            if (blockOn <= 0)
                CreateBlocks();
        }
        get
        {
            return blockOn;
        }
    }

    private static BlockGen instanse;

    public static BlockGen GetInstanse()
    {
        
        return instanse;
    }

    void Awake()
    {
        if (instanse == null)
        {
            instanse = this;
        }


    }

    public CellDescription[] GetCurrentBlocks()
    {
        CellDescription[] currentBlocks;
        currentBlocks = GetComponentsInChildren<CellDescription>();
        //Debug.Log("Current: " + currentBlocks.Length);
        return currentBlocks;
    }


    public void PrepareSave(List<BlockSaveData> blocks)
    {
        Clear();
        foreach (BlockSaveData b in blocks)
        {
//            Debug.Log(b.blockSpryteType);
            CreateConcreteBlock(b.blockId, new Vector3(b.blockX, b.blockY, b.blockZ), b.blockSpryteType);
        }
    }

    void StartChange(int type, CellDescription block)
    {
        StartCoroutine(ChangeSprite(type, block));
    }

    IEnumerator ChangeSprite(int type, CellDescription block)
    {
        yield return new WaitForSeconds(0.01f);
        block.ChangeSprite(type);
    }

    void OnDisable()
    {
        HomeCtrl.GetInstanse().OnPlay -= NewGame;
        PauseMgr.GetInstanse().OnHome -= RestartBlocks;

    }

    void Start()
    {
        HomeCtrl.GetInstanse().OnPlay += NewGame;
        PauseMgr.GetInstanse().OnHome += RestartBlocks;
        localStartX = -(((step * (maxBlocks - 1)) + blockWidth * maxBlocks) / 2);
        CreateBlocks();
    }

    void NewGame()
    {
        blockUndo = false;
        CreateBlocks();
    }

    public void RestartBlocks()
    {
        blockUndo = true;
        SwapBlocks();
    }

    public void CreateBlocks()
    {
        Clear();
        if (blockOn <= 0 && !blockUndo)
        {
            float posX = localStartX;
            for (int i = 0; i < maxBlocks; i++)
            {
                
                int randomIndex = UnityEngine.Random.Range(0, blockTypes.Count);
                Vector3 position = new Vector3(posX, 0f);
                posX += (step + blockWidth);
                if (singleCellsToSapwn <= 0)
                {
                    CreateConcreteBlock(randomIndex, position);

                }
                else
                {
                    CreateConcreteBlock(-1, position);
                   
                    singleCellsToSapwn--;
                }
                if (MapGen.GetInstanse() != null)
                    MapGen.GetInstanse().CallGameCheckDelay();
            }
//            blockOn = maxBlocks;
        }

    }

    GameObject CreateConcreteBlock(int index, Vector3 position = default(Vector3), int sprite = -1)
    {
        GameObject go;
        if (index == -1)
        {
            go = Instantiate(singleCell);
            go.GetComponent <CellDescription>().blockIndex = -1;
            go.GetComponent <SingleBlock>().isPowerUp = false;
            go.GetComponent <SingleBlock>().Init();
            go.SetActive(true);
        }
        else
        {
            go = Instantiate(blockTypes[index]);
            go.GetComponent <CellDescription>().blockIndex = index;
        }
        if (sprite == -1)
            go.GetComponent <CellDescription>().cellSpriteType = UnityEngine.Random.Range(1, HomeCtrl.spriteTypesCount);
        else
            StartChange(sprite, go.GetComponent <CellDescription>());
//            go.GetComponent <CellDescription>().ChangeSprite(sprite);
        go.transform.SetParent(transform);
        go.transform.localPosition = position;
        BlockOn++;
    

        return go;
    }

    public void SwapBlocks()
    {
        //*
        if (OnRemoveBlocks != null)
            OnRemoveBlocks();
        // */
       
    }

    void Clear()
    {
        Transform[] arr = GetComponentsInChildren<Transform>();
        for (int i = 1; i < arr.Length; i++)
        {
            Destroy(arr[i].gameObject);
        }
        blockOn = 0;

    }

    public void UndoBlock(int blockIndex, Vector3 blockLocalPosition, int blockSprite)
    {
        blockUndo = true;
        if (BlockOn == maxBlocks)
        {
            SwapBlocks();
            blockLocalPosition = Vector3.zero;
            blockLocalPosition.x = localStartX + step + blockWidth;
           
        }
        CreateConcreteBlock(blockIndex, blockLocalPosition, blockSprite);
        blockUndo = false;
    }

    public void DisableColliders()
    {
        CellDescription[] currentBlocks = GetCurrentBlocks();
        Debug.Log("currentBlocks " + currentBlocks.Length);
        foreach (CellDescription b in currentBlocks)
        {
            b.gameObject.GetComponent<BoxCollider>().enabled = false;
        }
    }

    public void EnableColliders()
    {
        Debug.Log("EnableColliders");
        CellDescription[] currentBlocks = GetCurrentBlocks();
        foreach (CellDescription b in currentBlocks)
        {
            b.gameObject.GetComponent<BoxCollider>().enabled = true;
        }
    }
}
