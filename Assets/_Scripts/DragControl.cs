﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragControl : MonoBehaviour
{
    protected Vector3 startPos;
    public CellDescription scr;


    void Start()
    {
        SphereCollider c = gameObject.AddComponent<SphereCollider>();
        c.radius = 0.75f;
        Destroy(gameObject.GetComponent<BoxCollider>());
    }

    public void OnMouseDown()
    {
        startPos = transform.parent.position;
    }

    public void OnMouseDrag()
    {
        if (PowerUpInfo.onPause)
            return;
        
        //Vector3 fingerPos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
        //Vector3 newPos = fingerPos + Vector3.up * Mathf.Clamp((fingerPos.y - (-2)) * 0.7f, 0.5f, 1.7f);


        Vector3 newPos = Camera.main.ScreenToWorldPoint(Input.mousePosition) + Vector3.up * 0.66f;
        if (scr.topDistance > 0)
        {
            newPos += Vector3.up * 0.5f;
        }


        newPos.z = transform.parent.position.z;
        transform.parent.position = newPos;
        scr.RotateBlock();
//        MapGen.GetInstanse().HideHint();
    }


    public void SetParentScript(CellDescription parent)
    {
        scr = parent;
    }

    public void OnMouseUp()
    {
        
        scr.OnMouseUp2(startPos);

    }
}
