﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class BlockSaveData
{
    public int blockId;
    public int blockSpryteType;
    public float blockX, blockY, blockZ;
}
