﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if !UNITY_ANDROID
using UnityEditor;
#endif  // UNITY_ANDROID

using System;



[Serializable]
public class SaveForma
{
    public int score;
    public List<int> savedPowerUps;
    [SerializeField]
    public List<CellSaveData> usedCells;
    [SerializeField]
    public List<BlockSaveData> currentBlocks;

}
