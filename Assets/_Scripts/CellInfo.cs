﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CellInfo : MonoBehaviour
{
    public int blockId, cellId;
    public bool used = false;

    Color defaultColor = new Color(169f / 225f, 169f / 225f, 169f / 225f, 0.75f);
       
    //TODO: fix
    //new Color(189 / 255f, 189 / 255f, 189 / 255f, 0.75f);
    Color spriteOver = Color.white;
    //stores color of the block if used
    Color tempColor = default(Color);
    Color blocked = Color.gray;

    Sprite defaultSprite;
    Sprite tempSprite = default(Sprite);
    public int spriteType;
    Renderer cellRend;
    SpriteRenderer sRend;
    Vector3 defaultPos;
    Vector3 defaultScale;
    bool hintRunning = false;

    void Awake()
    {
        cellRend = gameObject.GetComponent<Renderer>();
        sRend = gameObject.GetComponent<SpriteRenderer>();
        defaultSprite = sRend.sprite;
    }

    IEnumerator IResetter;

    public void StartHint()
    {
//        if (!hintRunning)
        StopCoroutine("IHint");
        StartCoroutine("IHint");
    }

    IEnumerator IHint()
    {
        hintRunning = true;
        int counter = 0;
        float startAlpha = 0.2f;
        Color c = cellRend.material.color;
        c.a = startAlpha;
        cellRend.material.color = c;
        bool startIncrease = true;
//        while (counter < 10)
        while (hintRunning)
        {
            if (startIncrease)
            {
                if (cellRend.material.color.a >= 0.8f)
                {
                    startIncrease = false;
                    counter++;
                }
                else
                {
                    c = cellRend.material.color;
                    c.a += 0.05f;
                    cellRend.material.color = c;
                }
            }
            else
            {
                if (cellRend.material.color.a <= 0.1f)
                {
                    startIncrease = true;
                    counter++;
                }
                else
                {
                    c = cellRend.material.color;
                    c.a -= 0.05f;
                    cellRend.material.color = c;
                }
            }
            yield return new WaitForSeconds(0.11f);

        }
//        cellRend.material.color = Color.black;
//        SetColor(Color.white, -1, true);
//        RestoreColor();
    }

    void Start()
    {
        
       

        defaultPos = transform.position;
        defaultScale = transform.localScale;
    }

    public void RestoreCell()
    {
        hintRunning = false;
        transform.position = defaultPos;
        transform.localScale = defaultScale;
//        Debug.Log("StopAllCoroutines");
//        StopAllCoroutines();
//        StopCoroutine("IHint");
    }

    public void StartCollapse()
    {
        StartCoroutine(ICollapseCell());
    }

    IEnumerator ICollapseCell()
    {
        while (gameObject.transform.localScale.x > 0f && gameObject.transform.localScale.y > 0f)
        {
            gameObject.transform.localScale -= new Vector3(0.05f, 0.05f, 0f);
            yield return new WaitForSeconds(0.001f);
        }
    }

    public void InitColor()
    {
        if (cellRend == null || sRend == null)
        {
            cellRend = gameObject.GetComponent<Renderer>();
            sRend = gameObject.GetComponent<SpriteRenderer>();

        }
        cellRend.material.color = defaultColor;
    }

    public void SetColor(Color choosenColor, int spriteType, bool saveColor = false, bool setBlocked = false)
    {
        if (!used)
        {
            cellRend.material.color = choosenColor;
            SetSprite(spriteType);
            
        }
        else
            cellRend.material.color = blocked;
        if (saveColor)
        {
            tempColor = choosenColor;
         
            tempSprite = HomeCtrl.GetInstanse().BBSprites[spriteType, cellId];
            this.spriteType = spriteType;

        }

        if (setBlocked)
            cellRend.material.color = blocked;
    }

    public void RestoreColor()
    {
        if (cellRend == null || sRend == null)
        {
            cellRend = gameObject.GetComponent<Renderer>();
            sRend = gameObject.GetComponent<SpriteRenderer>();
            
        }
//        Debug.Log("StopAllCoroutines");
//        StopAllCoroutines();

//        StopCoroutine("IHint");
        hintRunning = false;
            
        //*/
        Color c = cellRend.material.color;
        c.a = 1f;
        cellRend.material.color = c;
        //*/
      
        if (object.Equals(tempColor, default(Color)))
            cellRend.material.color = defaultColor;
        else
            cellRend.material.color = tempColor;

        if (object.Equals(tempSprite, default(Sprite)))
        {
            sRend.sprite = defaultSprite;
        }
        else
        {
            sRend.sprite = tempSprite;
        }
    }

    IEnumerator IResetCell()
    {
//        Debug.Log("IResetCell");
        used = false;
        cellRend.material.color = defaultColor;
//        sRend.sprite = defaultSprite;
        while (sRend.color.a > 0f)
        {
            sRend.color = new Color(sRend.color.r, sRend.color.b, sRend.color.g, sRend.color.a - 0.05f);
//            yield return new WaitForSeconds(0.01f);
            yield return new WaitForEndOfFrame();
        }
//        Debug.Log("endIResetCell");
        ResetCell();
        while (sRend.color.a < 1f)
        {
            sRend.color = new Color(sRend.color.r, sRend.color.b, sRend.color.g, sRend.color.a + 0.05f);
//            yield return new WaitForSeconds(0.01f);
            yield return new WaitForEndOfFrame();
        }
//        IResetter.Reset();
    }

    public void CellResetter()
    {
        
//        IResetter = IResetCell();

        StartCoroutine(IResetCell());
    }

    public void ResetCell()
    {
        if (cellRend == null || sRend == null)
        {
            cellRend = gameObject.GetComponent<Renderer>();
            sRend = gameObject.GetComponent<SpriteRenderer>();

        }
        cellRend.material.color = defaultColor;
        sRend.sprite = defaultSprite;
        tempColor = default(Color);
        tempSprite = default(Sprite);
        used = false;
        sRend.color = new Color(sRend.color.r, sRend.color.b, sRend.color.g, 1f);
    }

    public void TCellResetter(Tutorial t)
    {
        StartCoroutine(TResetCell(t));
    }

   

    IEnumerator TResetCell(Tutorial t)
    {
        used = false;
        while (sRend.color.a > 0f)
        {
            sRend.color = new Color(sRend.color.r, sRend.color.b, sRend.color.g, sRend.color.a - 0.05f);
//            yield return new WaitForSeconds(0.01f);
            yield return new WaitForEndOfFrame();
        }
        ResetCell();
        while (sRend.color.a < 1f)
        {
            sRend.color = new Color(sRend.color.r, sRend.color.b, sRend.color.g, sRend.color.a + 0.05f);
//            yield return new WaitForSeconds(0.01f);
            yield return new WaitForEndOfFrame();
        }
        t.cleared = true;

    }

    void SetSprite(int spriteType)
    {
        sRend.sprite = HomeCtrl.GetInstanse().BBSprites[spriteType, cellId];
    }

    public void StartClearSprite()
    {
        transform.GetChild(0).gameObject.SetActive(true);
    }
    /*/
    public  IEnumerator ISetClearSprite()
    {
        sRend.sprite = HomeCtrl.GetInstanse().clearSprites[blockId];
        yield return WaitForEndOfFrame();
    }
    //*/
}
