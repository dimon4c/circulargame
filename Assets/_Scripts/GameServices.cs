﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class GameServices {

	//----------------------------------------------
	static bool LoggedIn = false;
	static bool AttempedToLoginToday = false;
	#if UNITY_ANDROID
	const string leaderboard = "CgkIvaTDwJMXEAIQBQ";
	#elif UNITY_IPHONE
	const string leaderboard = "yyy";
	#elif UNITY_EDITOR
	const string leaderboard = "xxx";
	#endif

	//----------------------------------------------
	public GameServices()
	{
	#if UNITY_ANDROID
	PlayGamesPlatform.Activate();
	PlayGamesPlatform.DebugLogEnabled = false;
	#endif
	FirstLogin();
}
//----------------------------------------------
void Authenticate()
{			
	// authenticate user:
	Social.localUser.Authenticate((bool success) => {
		// handle success or failure
		LoggedIn = success;
		if (!success)
		{
			PlayerPrefs.SetInt("declined_play_services", 1);
		}
		AttempedToLoginToday = true;
	});
}
//----------------------------------------------
public void FirstLogin()
{
	int earlierDeclined = PlayerPrefs.GetInt("declined_play_services", 0);
	if (earlierDeclined == 1) 
		return;
	if (AttempedToLoginToday && !LoggedIn)
		return;
	Authenticate();

}
//----------------------------------------------
public void RequestedLogin()
{		
	Authenticate();
}
//----------------------------------------------
public void ReportScore(int score)
{		
	if (!LoggedIn) return;
	Social.ReportScore (score, leaderboard, (bool success) => {
		// handle success or failure
	});

}
//----------------------------------------------
public void Report100Points()
{
	ReportAchievement("ach_100_points");
}
//----------------------------------------------
public void Report1000Points()
{
	ReportAchievement("ach_1000_points");
}
//----------------------------------------------
public void Report10000Points()
{
	ReportAchievement("ach_10000_points");
}
//----------------------------------------------
public void ReportAchievement(string name)
{
	if (!LoggedIn) return;
	Social.ReportProgress(name, 100.0f, (bool success) => {
		// handle success or failure
	});
}
//----------------------------------------------
public void ShowLeaderboard()
{        
	#if UNITY_ANDROID
	if (LoggedIn) {
		((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(leaderboard);
	} else {
		// authenticate user:
		Social.localUser.Authenticate ((bool success) => {
			LoggedIn = success;
			if (success) {
				((PlayGamesPlatform)Social.Active).ShowLeaderboardUI (leaderboard);
				PlayerPrefs.SetInt("declined_play_services", 0);
			}
		});
	} 
	#endif

	#if UNITY_IPHONE
	if (LoggedIn) {
	(Social.Active).ShowLeaderboardUI();
	} else {
	// authenticate user:
	Social.localUser.Authenticate ((bool success) => {
	LoggedIn = success;
	if (success) {
	(Social.Active).ShowLeaderboardUI ();
	PlayerPrefs.SetInt("declined_play_services", 0);
	}
	});
	} 
	#endif
}

}
