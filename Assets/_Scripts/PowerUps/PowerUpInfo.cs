﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PowerUps
{
    SingleBlock,
    LuckyPower,
    HammerPower,
    BombPower,
    UndoPower,
    Swap}
;

public class PowerUpInfo
{
    public static int playerHiScore = 0;
    public static int playerCurrentScore = 0;
    public static int playerStars = 0;
    public static List<PowerUps> activated = new List<PowerUps>();
    //changed
    public static int[] powerCost = { 5, 50, 20, 30, 5, 25 };
    public static bool onPause = false;
    public static bool gameOver = false;

    public static bool CheckAvailability(PowerUps powerUp)
    {
        bool available = false;
        //Debug.Log(playerStars);
        if (powerCost[(int)powerUp] <= playerStars)
        {
//            if (activated == null)
//                activated = new List<PowerUps>();

            if (!activated.Contains(powerUp))
                activated.Add(powerUp);
            playerStars -= powerCost[(int)powerUp];
            PlayerPrefs.SetInt("Money", playerStars);
            available = true;
        }
        HomeCtrl.GetInstanse().UpdateMoney(playerStars);
        return available;
    }

    public static void AddMoney(int amount)
    {
        /*
		if (HomeCtrl.GetInstanse().DoubleCoinsForOneGame) {
			playerStars += (amount * 2);
		} else {
			playerStars += amount;
		}
		*/
        playerStars += amount;
        SaveMoney();
    }

    public static void SaveMoney()
    {
        PlayerPrefs.SetInt("Money", playerStars);
    }

    public static void RemoveMoney(int amount)
    {
        playerStars -= amount;
        SaveMoney();
    }

    public static void RemoveUsed(PowerUps powerUp)
    {
        if (activated.Contains(powerUp))
        {
            activated.Remove(powerUp);
        }
    }

    public static void RemovePowerUp(PowerUps powerUp)
    {
        if (activated.Contains(powerUp))
        {
            activated.Remove(powerUp);
            playerStars += powerCost[(int)powerUp];
            PlayerPrefs.SetInt("Money", playerStars);
        }
        HomeCtrl.GetInstanse().UpdateMoney(playerStars);
    }

    public static void LoadScore()
    {
        playerHiScore = PlayerPrefs.GetInt("Score", 0);
    }

    public static void CheckScore()
    {
        if (playerHiScore < playerCurrentScore)
        {
            playerHiScore = playerCurrentScore;
			HomeCtrl.GetInstanse().GameServices.ReportScore(playerCurrentScore);
            UpdateScore();
        }
    }

    public static void UpdateScore()
    {
        PlayerPrefs.SetInt("Score", PowerUpInfo.playerHiScore);
    }

    public static void ResetCurrentScore()
    {
        playerCurrentScore = 0;
    }
}
