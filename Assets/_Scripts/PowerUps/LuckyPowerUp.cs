﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuckyPowerUp : MonoBehaviour
{
    const int singleCellsToSpawn = 6;
    public PowerUps power = PowerUps.LuckyPower;

    void Start()
    {
        PauseMgr.GetInstanse().OnHome += OnWentToMain;
    }

    void OnDisable()
    {
        PauseMgr.GetInstanse().OnHome -= OnWentToMain;
    }

    public void OnMouseDown()
    {

        PowerUpActivator.GetInstanse().StopShake();
    }

    void OnWentToMain()
    {
        Destroy(gameObject.transform.parent.gameObject);
    }

    void OnMouseUp()
    {
        if (PowerUpInfo.onPause)
            return;
        BlockGen.GetInstanse().singleCellsToSapwn = singleCellsToSpawn;
        PowerUpInfo.RemoveUsed(power);
        MapGen.GetInstanse().CheckGameOver();
        Destroy(gameObject.transform.parent.gameObject);
    }
	
}
