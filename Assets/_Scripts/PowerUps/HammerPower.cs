﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerPower : PowerDescription
{
    //public PowerUps power = PowerUps.HammerPower;
    Indexes[] blockIndexes;

    public Indexes[] BlockIndexes
    {
        set
        { 
            blockIndexes = new []
            { 
                new Indexes(0, 0), new Indexes(0, 1), new Indexes(0, -1),
                new Indexes(1, 0), new Indexes(1, 1), new Indexes(1, -1),
                new Indexes(-1, 0), new Indexes(-1, 1), new Indexes(-1, -1)
            };
        }
        get
        {
            if (blockIndexes == null)
                blockIndexes = new []
                {
                    new Indexes(0, 0), new Indexes(0, 1), new Indexes(0, -1),
                    new Indexes(1, 0), new Indexes(1, 1), new Indexes(1, -1),
                    new Indexes(-1, 0), new Indexes(-1, 1), new Indexes(-1, -1)
                };
            return blockIndexes;
        }
    }

    public override void Start()
    {
        power = PowerUps.HammerPower;
        cellSpriteType = 0;
        this.gameObject.transform.GetChild(0).GetComponent<DragControlPower>().SetParentScript(this);
        mapHolder = GameObject.Find("MapHolder");
        topDistance = MapGen.GetInstanse().GetMapDistance();
        PauseMgr.GetInstanse().OnHome += OnWentToMain;
    }

    protected override void OnTriggerEnter(Collider other)
    {
        imgHolder.SetActive(false);
        collidedCell = other.gameObject.GetComponent<CellInfo>();
//        MapGen.GetInstanse().HammerHighlight(collidedCell, color);
        MapGen.GetInstanse().CheckBlock(BlockIndexes, cellSpriteType, collidedCell.blockId, collidedCell.cellId, color);
    }
}
