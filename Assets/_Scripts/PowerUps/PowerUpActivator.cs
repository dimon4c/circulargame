﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PowerUpActivator: MonoBehaviour
{
    Animator anim;
    public GameObject 
        singleBlock,
        luckyPower,
        hammerPower,
        bombPower,
        undoPower, 
        swapPower;
    public List<GameObject> posList;
    private static PowerUpActivator instanse;

    public static PowerUpActivator GetInstanse()
    {

        return instanse;
    }

   
    void Awake()
    {
        if (instanse == null)
        {
            instanse = this;
        }
        HomeCtrl.GetInstanse().OnPlay += CheckPowerUps;
        anim = GetComponent<Animator>();
    }

    void OnDisable()
    {
        HomeCtrl.GetInstanse().OnPlay -= CheckPowerUps;
    }

    void Start()
    {
//        CheckPowerUps();
    }

    public void StartShake()
    {

        anim.Play("Bounce");
//        StartCoroutine(IShake());
    }

    /*
    IEnumerator IShake()
    {
        anim.Play("Bounce");

        yield return new WaitForSeconds(1f);
        anim.StopPlayback();
        anim.Play("Idle");
      
        
    }
    */

    public void StopShake()
    {
        anim.StopPlayback();
        anim.Play("Idle");
    }

    public  void CheckPowerUps()
    {
        
        if (PowerUpInfo.activated == null)
            PowerUpInfo.activated = new List<PowerUps>();
        int i = 0;
        foreach (PowerUps id in PowerUpInfo.activated)
        {
            //Debug.Log(id);
            GameObject go;
            switch (id)
            {
                case PowerUps.SingleBlock:
                    go = Instantiate(singleBlock);
                   
                    break;
                case PowerUps.LuckyPower:
                    go = Instantiate(luckyPower);
                   
                    break;
                case PowerUps.HammerPower:
                    go = Instantiate(hammerPower);
                   
                    break;
                case PowerUps.BombPower:
                    go = Instantiate(bombPower);
                   
                    break;
                case PowerUps.UndoPower:
                    go = Instantiate(undoPower);

                    break;
                case PowerUps.Swap:
                    go = Instantiate(swapPower);

                    break;
                default:
                    go = null;
                    break;
            }

            if (go != null)
            {
                go.transform.position = posList[i].transform.position;
                go.transform.SetParent(transform);
                go.SetActive(true);
            }
            else
                Debug.Log("PowerUp id doesn't fit any cases");
            i++;
        }
    }

    public bool PowerUpsAvailabe()
    {
        
        Transform[] powers = GetComponentsInChildren<Transform>();
        //Debug.Log("powers.Length " + powers.Length);

        if (PowerUpInfo.activated.Count == 1 && PowerUpInfo.activated.Contains(PowerUps.UndoPower))
        {
            if (!UndoPowerUp.GetInstanse().undoAvailable)
                return false;
        }
        if (PowerUpInfo.activated.Count > 0)
            return true;
        else
            return false;
    }

    public void StartMoveBomb(Vector3 fromPos, Vector3 toPos)
    {
        Debug.Log("StartMoveBomb");
        GameObject go = Instantiate(bombPower);
        Destroy(go.GetComponent<BoxCollider>());
        go.SetActive(true);
        HomeCtrl.GetInstanse().StartMove(go, fromPos, toPos);
    }

    public void StartMoveHammer(Vector3 fromPos, Vector3 toPos)
    {
        Debug.Log("StartMoveHammer");
        GameObject go = Instantiate(hammerPower);
        Destroy(go.GetComponent<BoxCollider>());
        go.SetActive(true);
        HomeCtrl.GetInstanse().StartMove(go, fromPos, toPos);
    }

}
