﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UndoPowerUp : MonoBehaviour
{
    public int currentBlockId;
    public int blockSprite;
    public List<CellInfo> setCells;
    public Vector3 blockLocalPosition;
    public bool undoAvailable = true;
    public PowerUps power = PowerUps.UndoPower;
    private static UndoPowerUp instanse;

    public static UndoPowerUp GetInstanse()
    {

        return instanse;
    }

    void Awake()
    {
       
        if (instanse == null)
        {
            instanse = this;
        }

    }

    void Start()
    {
        PauseMgr.GetInstanse().OnHome += OnWentToMain;
    }

    void OnDisable()
    {
        PauseMgr.GetInstanse().OnHome -= OnWentToMain;
    }

    void OnWentToMain()
    {
        Destroy(gameObject.transform.parent.gameObject);
    }

    void UndoMove()
    {
        if (setCells.Count > 0 && undoAvailable)
        {
            MapGen.GetInstanse().BombCells(setCells);
            BlockGen.GetInstanse().UndoBlock(currentBlockId, blockLocalPosition, blockSprite);
            PowerUpInfo.RemoveUsed(power);
            MapGen.GetInstanse().CallGameCheckDelay();
            Destroy(gameObject.transform.parent.gameObject);
        }
    }

    public void OnMouseDown()
    {

        PowerUpActivator.GetInstanse().StopShake();
    }

    void OnMouseUp()
    {
        if (PowerUpInfo.onPause)
            return;
        UndoMove();

    }

}
