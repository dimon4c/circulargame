﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpsManager : MonoBehaviour
{

    //added
    public GameObject[] pwrOnImages = new GameObject[6];

    public static PowerUpsManager Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void ShowSaved()
    {
        foreach (PowerUps p in PowerUpInfo.activated)
            pwrOnImages[(int)p].SetActive(true);
        
    }


    public void RequestPowerUp(int power)
    {
        PowerUps powerUp = (PowerUps)power;

        //added start
        //----------------------------------------
//        Debug.Log(PowerUpInfo.activated.Count);
//        Debug.Log(pwrOnImages[power].activeSelf);

        if (PowerUpInfo.activated.Count >= 3 && !pwrOnImages[power].activeSelf)
            return;

        if (!pwrOnImages[power].activeSelf)
        {
            if (CheckPowerUp(powerUp))
                pwrOnImages[power].SetActive(!pwrOnImages[power].activeSelf);
        }
        else
        {
            PowerUpInfo.RemovePowerUp(powerUp);
            pwrOnImages[power].SetActive(false);
        }


			
        //-----------------------------------------
        //added end


    }

    //added...
    public void Reset()
    {
//        PowerUpInfo.activated.Clear();
        for (int i = 0; i < 6; i++)
        {
            pwrOnImages[i].SetActive(false);
        }
    }


    bool CheckPowerUp(PowerUps powerUp)
    {
        return PowerUpInfo.CheckAvailability(powerUp);
    }
}
