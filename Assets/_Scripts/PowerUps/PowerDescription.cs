﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerDescription : MonoBehaviour
{
    protected  Color color = new Color(125f / 255f, 125f / 255f, 125f / 255f, 0.9f);
    protected CellInfo collidedCell;
    protected float topDistance;
    protected GameObject mapHolder;
    public GameObject imgHolder;
    public  int cellSpriteType = 0;
    public PowerUps power = PowerUps.BombPower;
    GameObject bombHolder, explosion;
    Vector3 SpawnPos = new Vector3(0f, 8.5f);

    public virtual void Start()
    {
       
        this.gameObject.transform.GetChild(0).GetComponent<DragControlPower>().SetParentScript(this);
        mapHolder = GameObject.Find("MapHolder");
        topDistance = MapGen.GetInstanse().GetMapDistance();
        PauseMgr.GetInstanse().OnHome += OnWentToMain;
        bombHolder = transform.FindChild("BombHolder").gameObject;
        explosion = bombHolder.transform.GetChild(1).gameObject;
    }

    void Awake()
    {
        
    }

    void OnDisable()
    {
        PauseMgr.GetInstanse().OnHome -= OnWentToMain;
    }

    protected void OnWentToMain()
    {
        Destroy(gameObject);
    }

    public virtual void OnMouseUp2(Vector3 startPos)
    {
        if (collidedCell == null || Vector3.Distance(transform.position, mapHolder.transform.position) > topDistance)
        {
            transform.position = startPos;

        }
        else
        {
            StartBobmCall();

        }
    }

    void StartBobmCall()
    {
        StartCoroutine(BombCall());
    }

    IEnumerator BombCall()
    {
        Vector3 jumpHeight = new Vector3(0f, 1.5f, 0f);
        SpawnPos.x = Random.Range(-3f, 3f);
        bombHolder.transform.position = SpawnPos;
//        Vector3 endPos = transform.position;
        Vector3 endPos = collidedCell.gameObject.transform.position;
//        endPos.y += 2.5f;
        bombHolder.SetActive(true);
        float moveStep = 0.2f;
        float scaleStep = 0.4f / (Vector3.Distance(endPos, bombHolder.transform.position) / moveStep);
        float rotateStep = -180f / (Vector3.Distance(endPos, bombHolder.transform.position) / moveStep);
        Transform bombTransform = bombHolder.transform.GetChild(0);
        while (Vector3.Distance(endPos, bombHolder.transform.position) > moveStep)
        {
            bombHolder.transform.position = Vector3.MoveTowards(bombHolder.transform.position, endPos, moveStep);
            bombTransform.localScale -= new Vector3(scaleStep, scaleStep, 0);
            bombTransform.Rotate(new Vector3(0f, 0f, 1f), rotateStep);
            yield return new WaitForEndOfFrame();
//            yield return new    WaitForSeconds(0.3f);
        }

        //*/
        endPos += jumpHeight;
        while (Vector3.Distance(endPos, bombHolder.transform.position) > jumpHeight.y / 3)
        {
            bombHolder.transform.position = Vector3.MoveTowards(bombHolder.transform.position, endPos, jumpHeight.y / 20);

            yield return new WaitForEndOfFrame();
//            yield return new    WaitForSeconds(2f);
        }
        endPos -= jumpHeight;
        while (Vector3.Distance(endPos, bombHolder.transform.position) > jumpHeight.y / 3)
        {
            bombHolder.transform.position = Vector3.MoveTowards(bombHolder.transform.position, endPos, jumpHeight.y / 10);

            yield return new WaitForEndOfFrame();
//            yield return new    WaitForSeconds(2f);
        }
        //*/
        explosion.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        MapGen.GetInstanse().BombCells();
        PowerUpInfo.RemoveUsed(power);
        if (UndoPowerUp.GetInstanse() != null)
            UndoPowerUp.GetInstanse().undoAvailable = false;
        MapGen.GetInstanse().CheckGameOver();
        Destroy(gameObject);
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        imgHolder.SetActive(false);
        collidedCell = other.gameObject.GetComponent<CellInfo>();
        MapGen.GetInstanse().BombHighlight(collidedCell, cellSpriteType, color);
    }

    public void S()
    {
        //Debug.Log(topDistance);
        //Debug.Log(Vector3.Distance(transform.position, mapHolder.transform.position));
        if (mapHolder == null)
        {
            
            mapHolder = GameObject.Find("MapHolder");
        }
        else if (Vector3.Distance(transform.position, mapHolder.transform.position) > topDistance)
        {
            collidedCell = null;
            imgHolder.SetActive(true);
            MapGen.GetInstanse().RestoreCellColors();
        }
    }

    //*/
    protected void OnTriggerExit(Collider other)
    {
        if (topDistance <= 0f)
            topDistance = MapGen.GetInstanse().GetMapDistance();
       
        if (Vector3.Distance(transform.position, mapHolder.transform.position) > topDistance)
        {
            collidedCell = null;
            imgHolder.SetActive(true);
        }
        if (collidedCell != null && collidedCell == other.gameObject.GetComponent<CellInfo>())
        {
            MapGen.GetInstanse().RestoreCellColors();
        }
       

    }
}
