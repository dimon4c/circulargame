﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragControlPower : MonoBehaviour
{

    protected Vector3 startPos;
    public PowerDescription scr;

    public void OnMouseDown()
    {
        startPos = transform.parent.position;
        PowerUpActivator.GetInstanse().StopShake();
    }

    public void OnMouseDrag()
    {
        if (PowerUpInfo.onPause)
            return;
        Vector3 newPos = Camera.main.ScreenToWorldPoint(Input.mousePosition) + Vector3.up;
        newPos.z = transform.parent.position.z;
        transform.parent.position = newPos;
        scr.S();

    }

   

    public void SetParentScript(PowerDescription parent)
    {
        scr = parent;
    }

    public void OnMouseUp()
    {
        scr.OnMouseUp2(startPos);

    }
}
