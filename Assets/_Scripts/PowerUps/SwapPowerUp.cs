﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapPowerUp : MonoBehaviour
{
    public PowerUps power = PowerUps.Swap;

    void Start()
    {
        PauseMgr.GetInstanse().OnHome += OnWentToMain;
    }

    void OnDisable()
    {
        PauseMgr.GetInstanse().OnHome -= OnWentToMain;
    }

    void OnWentToMain()
    {
        Destroy(gameObject.transform.parent.gameObject);
    }

    public void OnMouseDown()
    {

        PowerUpActivator.GetInstanse().StopShake();
    }

    void OnMouseUp()
    {
        if (PowerUpInfo.onPause)
            return;
        BlockGen.GetInstanse().SwapBlocks();
        PowerUpInfo.RemoveUsed(power);
        MapGen.GetInstanse().CheckGameOver();
        Destroy(gameObject.transform.parent.gameObject);
    }

}

