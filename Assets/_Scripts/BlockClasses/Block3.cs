using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block3 : CellDescription
{
    public override Indexes[] BlockIndexes
    {
       
        get
        {
            if (blockIndexes == null)
                blockIndexes = new []{ new Indexes(0, 1), new Indexes(0, 2), new Indexes(0, 0) };
            return blockIndexes;
//            { new Indexes(1, 0), new Indexes(-1, 0), new Indexes(1, 1) };
        }
    }

}