using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block9 : CellDescription
{
    public override Indexes[] BlockIndexes
    {
      
        get
        {
            
            blockIndexes = new []{ new Indexes(1, 0), new Indexes(-1, 0) };
            return blockIndexes;
//            { new Indexes(1, 0), new Indexes(-1, 0), new Indexes(1, 1) };
        }
    }

}