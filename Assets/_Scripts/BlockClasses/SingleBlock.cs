﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security.Cryptography.X509Certificates;

public class SingleBlock : CellDescription
{
    public bool isPowerUp = true;
    public GameObject imgHolder;
    public PowerUps power = PowerUps.SingleBlock;

    public override Indexes[] BlockIndexes
    {
        set
        { 
            blockIndexes = new []{ new Indexes(0, 0) };
        }
        get
        {
            if (blockIndexes == null)
                blockIndexes = new []{ new Indexes(0, 0) };
            return blockIndexes;
        }
    }

    public override void Start()
    {
        
        cellSpriteType = UnityEngine.Random.Range(1, HomeCtrl.spriteTypesCount);
        this.gameObject.transform.GetChild(0).GetComponent<DragControl>().SetParentScript(this);
        cellSprite = new List<Sprite>();
        for (int i = 0; i < blockSize; i++)
            cellSprite.Add(HomeCtrl.GetInstanse().BBSprites[cellSpriteType, i]);
       
//        CreateImage();
        if (!isPowerUp)
            CreateMap();
        mapHolder = GameObject.Find("MapHolder");
        defaultRotation = cellHolder.transform.rotation;
        topDistance = MapGen.GetInstanse().GetMapDistance();
        startLocalPos = transform.localPosition;
        startPos = transform.position;
        if (!isPowerUp)
            BlockGen.GetInstanse().OnRemoveBlocks += OnRemove;
        else
            PauseMgr.GetInstanse().OnHome += OnWentToMain;
        Init();
    }

    void OnDisable()
    {
        if (!isPowerUp)
            BlockGen.GetInstanse().OnRemoveBlocks -= OnRemove;
        else
            PauseMgr.GetInstanse().OnHome -= OnWentToMain;
    }

    protected void OnWentToMain()
    {
        Destroy(gameObject);
    }

    public   void Init()
    {
        
        if (isPowerUp)
            cellHolder.SetActive(false);
        else
            imgHolder.SetActive(false);
    }

    public override void RotateBlock()
    {
        if (mapHolder == null)
        {
            mapHolder = GameObject.Find("MapHolder");
            return;
        }
        if (topDistance <= 0)
            topDistance = MapGen.GetInstanse().GetMapDistance();
        /*/
        Vector3 point = mapHolder.transform.position;
        point.y = 0.0f;
        cellHolder.transform.LookAt(point, cellHolder.transform.position);

//        cellHolder.transform.LookAt(mapHolder.GetComponent<Transform>());
        //*/
        if (Vector3.Distance(imgHolder.transform.position, mapHolder.transform.position) <= maxRadius || Vector3.Distance(imgHolder.transform.position, mapHolder.transform.position) > topDistance)
        {
            if (isPowerUp)
                imgHolder.SetActive(true);
            else
                cellHolder.SetActive(true);
            MapGen.GetInstanse().RestoreCellColors();
        }
        if (MapGen.GetInstanse().currentHighlight.Count < 0)
        {
            if (isPowerUp)
            {
                imgHolder.SetActive(true);
               

                
            }
            else
                cellHolder.SetActive(true);
        }
        if (!isPowerUp)
        {
            Vector3 targetDir = mapHolder.transform.position - transform.position;


            float angle = Mathf.Atan2(targetDir.x, targetDir.y) * Mathf.Rad2Deg;

            Vector3 targetDir2 = mapHolder.transform.position - transform.position;


            transform.rotation = Quaternion.Euler(0.0f, 0.0f, -angle);

        }
        if (isPowerUp)
            PowerUpActivator.GetInstanse().StopShake();
        else
            MapGen.GetInstanse().HideHint();
        /*//
        Vector3 targetDir = mapHolder.transform.position - transform.position;


        float angle = Mathf.Atan2(targetDir.x, targetDir.y) * Mathf.Rad2Deg;

        Vector3 targetDir2 = mapHolder.transform.position - transform.position;


        transform.rotation = Quaternion.Euler(0.0f, 0.0f, -angle);


        //*/
    }

    public override void OnRemove()
    {
        if (!isPowerUp)
        {
            BlockGen.GetInstanse().OnRemoveBlocks -= OnRemove;
            BlockGen.GetInstanse().BlockOn--;
            if (UndoPowerUp.GetInstanse() != null)
            {
                UndoPowerUp.GetInstanse().blockLocalPosition = startLocalPos;
                UndoPowerUp.GetInstanse().currentBlockId = blockIndex;
                UndoPowerUp.GetInstanse().blockSprite = cellSpriteType;
            }

//            MapGen.GetInstanse().CheckGameOver(this);
        }
        else
        {
            if (UndoPowerUp.GetInstanse() != null)
            {
                UndoPowerUp.GetInstanse().undoAvailable = false;
            }
            PowerUpInfo.RemoveUsed(power);
//            MapGen.GetInstanse().CheckGameOver();
        }
        Destroy(gameObject);
    }

    protected override void OnTriggerEnter(Collider other)
    {
//        Debug.Log("enter");
        if (isPowerUp)
            imgHolder.SetActive(false);
        else
            cellHolder.SetActive(false);
        //            collided = true;
        collidedCell = other.gameObject.GetComponent<CellInfo>();

        MapGen.GetInstanse().CheckBlock(BlockIndexes, cellSpriteType, collidedCell.blockId, collidedCell.cellId, blockColor);

        //*/
    }


    //*/
    protected override   void OnTriggerExit(Collider other)
    {
        //*/
        if (other.gameObject.GetComponent<CellInfo>() == collidedCell)
        {
            collided = false;
            collidedCell = null;
            MapGen.GetInstanse().RestoreCellColors();
        }
        // Restore status
        //*/
    }

}
