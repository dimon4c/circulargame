using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block14 : CellDescription
{
    public override Indexes[] BlockIndexes
    {
        set
        { 
            blockIndexes = new []{ new Indexes(1, 0), new Indexes(-1, 0) };
        }
        get
        {
            if (blockIndexes == null)
                blockIndexes = new []{ new Indexes(1, 3), new Indexes(0, 3), new Indexes(1, 2), new Indexes(1, 1), new Indexes(1, 0)  };
            return blockIndexes;
//            { new Indexes(1, 0), new Indexes(-1, 0), new Indexes(1, 1) };
        }
    }

}