﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockA : CellDescription
{
    public override Indexes[] BlockIndexes
    {
        set
        { 
            blockIndexes = new []{ new Indexes(0, 0), new Indexes(0, 1), new Indexes(-1, 0) };
        }
        get
        {
            if (blockIndexes == null)
                blockIndexes = new []{ new Indexes(0, 0), new Indexes(0, 1), new Indexes(-1, 0), new Indexes(1, 1) };
            return blockIndexes;
        }
    }


}
