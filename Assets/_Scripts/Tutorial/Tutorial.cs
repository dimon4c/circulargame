﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Tutorial : MonoBehaviour
{
    void Start()
    {
        startY += transform.position.y;
        StartTutorial();
    }

    void StartTutorial()
    {
        HomeCtrl.GetInstanse().GiveTutorialMoney = true;
        if (PlayerPrefs.GetInt("nightmode", 0) == 0)
        {
           
            HomeCtrl.GetInstanse().SwitchTheme();
        }
        CreateMap();
        PrepareLine();
        SpawnLineBlock();
        HomeCtrl.GetInstanse().demoStep1.SetActive(true);
        HomeCtrl.GetInstanse().demoCaption1.SetActive(true);
        HomeCtrl.GetInstanse().videobtnx2.SetActive(false);
        PauseMgr.GetInstanse().pauseBtn.SetActive(false);
    }

    void EndTutorial()
    {
        PauseMgr.GetInstanse().pauseBtn.SetActive(true);
        HomeCtrl.GetInstanse().videobtnx2.SetActive(true);
        Restart();
        HomeCtrl.GetInstanse().demoStep1.SetActive(false);
        HomeCtrl.GetInstanse().demoCaption1.SetActive(false);
        HomeCtrl.GetInstanse().demoStep2.SetActive(false);
        HomeCtrl.GetInstanse().demoCaption2.SetActive(false);
    }

   


}
