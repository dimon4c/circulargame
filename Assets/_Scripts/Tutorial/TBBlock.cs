﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TBBlock : TCellDescription
{
    public override Indexes[] BlockIndexes
    {

        get
        {
            if (blockIndexes == null)
                blockIndexes = new []
                { new Indexes(0, 0), new Indexes(0, 1)
                   
                };
            return blockIndexes;
            //            { new Indexes(1, 0), new Indexes(-1, 0), new Indexes(1, 1) };
        }
    }


    public override void Start()
    {
        cellSpriteType = 8;
        this.gameObject.transform.GetChild(0).GetComponent<TDragCtrl>().SetParentScript(this);
        cellSprite = new List<Sprite>();
        for (int i = 0; i < blockSize; i++)
            cellSprite.Add(HomeCtrl.GetInstanse().BBSprites[cellSpriteType, i]);
        CreateMap();
        mapHolder = GameObject.Find("Tutorial");
        tScript = mapHolder.GetComponent<Tutorial>();
        defaultRotation = cellHolder.transform.rotation;
        startLocalPos = transform.localPosition;
        topDistance = tScript.GetMapDistance();

    }
}
