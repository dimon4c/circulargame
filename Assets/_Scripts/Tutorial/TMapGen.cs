﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Tutorial : MonoBehaviour
{
    List<GameObject> toEnable;
    bool line = false;
    bool block = false;
    const int blockSize = 8;
    const int blockCount = 8;
    int fixVal = 4;
    float startY = 0.55f;
    const float cellSpaceY = 0.306f;
    const float rotationStep = 45f;
    public bool cleared = false;
    [Header("CellPrefabs")]
    [Tooltip("Prefabs have to be set in ascending order starting from the smallest")]
    public GameObject[] cellPrefabs = new GameObject[blockSize];
    CellInfo[,] gameCells = new CellInfo[blockCount, blockSize];
    public List<CellInfo> currentHighlight;

    public void PrepareBlock()
    {
  
        for (int i = 0; i < blockCount; i++)
        {
            if (i == 3 || i == 4)
                continue;
            gameCells[fixVal, i].SetColor(Color.white, 5, true);
            gameCells[fixVal, i].used = true;
        }
        for (int i = 0; i < blockCount; i++)
        {
            for (int k = 0; k < blockSize; k++)
            {
                if (i != fixVal)
                    gameCells[i, k].used = true;
            }
        }
    }

    void PrepareLine()
    {
       
        for (int i = 0; i < blockCount; i++)
        {
            if (i == 3 || i == 4)
                continue;
            gameCells[i, fixVal].SetColor(Color.white, 8, true);
            gameCells[i, fixVal].used = true;
        }
        for (int i = 0; i < blockCount; i++)
        {
            for (int k = 0; k < blockSize; k++)
            {
                if (k != fixVal)
                    gameCells[i, k].used = true;
            }
        }
    }

    void CreateMap()
    {
        for (int i = 0; i < blockCount; i++)
        {
            for (int k = 0; k < blockSize; k++)
            {
                GameObject cell = Instantiate(cellPrefabs[k]);
                float height = startY + cellSpaceY * k;
                cell.transform.position = new Vector3(0f, height);
                cell.name = "blockId " + i + " cellId " + k;
                cell.transform.SetParent(transform);


                gameCells[i, k] = cell.GetComponent<CellInfo>();
                gameCells[i, k].blockId = i;
                gameCells[i, k].cellId = k;
                gameCells[i, k].InitColor();
            }
            transform.Rotate(0f, 0f, rotationStep);
        }
    }

    public bool CheckBlock(Indexes[] blockIndexes, int cellSpriteType, int blockId, int cellId, Color color = default(Color))
    {
        bool available = true;
        if (object.Equals(color, default(Color)))
            color = Color.black;
        RestoreCellColors();
        currentHighlight = new List<CellInfo>();
        Indexes[] blockIndexesConv = PrepareByCell(blockIndexes);
        for (int i = 0; i < blockIndexesConv.Length; i++)
        {
            Indexes cellIndex = FindCell(blockIndexesConv[i], blockId, cellId);
            if (cellIndex.byPos > blockSize - 1 || cellIndex.byPos < 0)
            {
                available = false;
            }
            else
            {

                //                Debug.Log("gameCells " + gameCells.GetLength(0));
                //                Debug.Log("gameCells " + gameCells.GetLength(1));
                currentHighlight.Add(gameCells[cellIndex.byBlock, cellIndex.byPos]);
                gameCells[cellIndex.byBlock, cellIndex.byPos].SetColor(color, cellSpriteType);

            }
        }
        foreach (CellInfo cell in currentHighlight)
        {
            if (cell.used)
            {
                available = !cell.used;
                break;
            }
        }

        return available;
    }

    public float GetMapDistance()
    {
        if (gameCells[0, blockSize - 1] != null)
        {
            GameObject lastblockCell = gameCells[0, blockSize - 1].gameObject;
            return Vector3.Distance(transform.position, lastblockCell.transform.position) + lastblockCell.transform.lossyScale.y;
        }
        else
            return 0f;
    }

    public void RestoreCellColors()
    {


        if (currentHighlight != null)
        {
            foreach (CellInfo cell in currentHighlight)
            {
                cell.RestoreColor();
            }
        }
    }

    int GetMinCell(Indexes[] BlockIndexes)
    {
        int minCellIndex = 0;
        foreach (Indexes i in BlockIndexes)
        {
            if (i.byPos < minCellIndex)
                minCellIndex = i.byPos;
        }
        return minCellIndex;
    }

    Indexes[] PrepareByCell(Indexes[] BlockIndexes)
    {
        Indexes[] cellIndexes = new Indexes[BlockIndexes.Length];
        int minCellIndex = GetMinCell(BlockIndexes);
        for (int i = 0; i < BlockIndexes.Length; i++)
        {
            cellIndexes[i] = BlockIndexes[i];
            cellIndexes[i].byPos = BlockIndexes[i].byPos - minCellIndex;
        }
        return cellIndexes;
    }

    Indexes FindCell(Indexes indexVector, int blockIdOrigin, int cellIdOrigin)
    {
        Indexes returnIdex;
        int blockIdCalc = blockIdOrigin + indexVector.byBlock;

        if (blockIdCalc < 0)
            blockIdCalc = blockCount - 1;
        else if (blockIdCalc > blockCount - 1)
            blockIdCalc = 0;

        returnIdex.byBlock = blockIdCalc;  
        returnIdex.byPos = cellIdOrigin + indexVector.byPos;


        return returnIdex;
    }

    public bool SetBlock(Indexes[] blockIndexes, int cellSpriteType, int blockId, int cellId, Color color = default(Color))
    {
        bool available;
        if (object.Equals(color, default(Color)))
            color = Color.black;
        if (CheckBlock(blockIndexes, cellSpriteType, blockId, cellId, color))
        {

            //set block
            for (int i = 0; i < currentHighlight.Count; i++)
            {
                currentHighlight[i].SetColor(color, cellSpriteType, true);
                currentHighlight[i].used = true;
            }
            CheckLines();
            available = true;

            AudioMgr.GetInstanse().PlaySound(0);
        }
        else
        {
            foreach (CellInfo cell in currentHighlight)
            {
                cell.RestoreColor();
            }
            available = false;
        }
        return available;
    }


    public void CheckLines()
    {
//        bool cleared = false;
        List<int> blocksId = FindFullBlocks();
        List<int> linesId = FindFullLines();
       
        foreach (int id in blocksId)
        {
            for (int i = 0; i < blockSize; i++)
            {
               
                gameCells[id, i].CellResetter();
            }
        }
      
        foreach (int id in linesId)
        {
            for (int i = 0; i < blockCount; i++)
            {
          
                if (i != blockCount - 1)
                    gameCells[i, id].CellResetter();
                else
                    gameCells[i, id].TCellResetter(this);
            }
        }
       
        if (spawnedLine && !spawnedBlock)
            StartCoroutine(Waiter());
        //*/
        if (spawnedLine && spawnedBlock)
            Invoke("EndTutorial", 0.7f);
        //*/
    }

    IEnumerator Waiter()
    {
        while (!cleared)
        {
            yield return new WaitForSeconds(0.5f);
        }
        PrepareBlock();
        SpawnBBlock();
        HomeCtrl.GetInstanse().demoStep1.SetActive(false);
        HomeCtrl.GetInstanse().demoCaption1.SetActive(false);

        HomeCtrl.GetInstanse().demoStep2.SetActive(true);
        HomeCtrl.GetInstanse().demoCaption2.SetActive(true);
    }

    List<int> FindFullBlocks()
    {
        List<int> blocksId = new List<int>();

        bool blockStrike = true;
        for (int k = 0; k < blockSize; k++)
        {
            
            if (!gameCells[fixVal, k].used)
            {
                blockStrike = false;
                Debug.Log(blockStrike);
                break;
            }
        }
        if (blockStrike)
            blocksId.Add(fixVal);
        

        return blocksId;
    }

    List<int> FindFullLines()
    {
        List<int> lineId = new List<int>();
       
        bool lineStrike = true;
        for (int k = 0; k < blockCount; k++)
        {
            if (!gameCells[k, fixVal].used)
            {
                
                lineStrike = false;
                Debug.Log(lineStrike);
                break;
            }
        }
        if (lineStrike)
            lineId.Add(fixVal);
        
        return lineId;
    }

  


    void Restart()
    {
        
//        PauseMgr.GetInstanse().tutorialFinished = true;

        PauseMgr.GetInstanse().HomeFromTutorial();
//        HomeCtrl.GetInstanse().ShowPowerUp();
//        Destroy(gameObject);
    }
}
