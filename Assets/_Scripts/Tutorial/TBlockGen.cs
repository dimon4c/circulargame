﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Tutorial : MonoBehaviour
{
    public    GameObject lineBlock, bBlock, blockPlace;
    bool spawnedLine = false;
    bool spawnedBlock = false;

    public void SpawnLineBlock()
    {
        GameObject go = Instantiate(lineBlock);
//        go.GetComponent<TCellDescription>().tScript = this;
        go.transform.position = new Vector3(0f, -2.4f, 0f);
        go.transform.SetParent(transform);
        spawnedLine = true;

    }

    public void SpawnBBlock()
    {
        GameObject go = Instantiate(bBlock);
//        go.GetComponent<TCellDescription>().tScript = this;
        go.transform.position = new Vector3(0f, -2.4f, 0f);
        go.transform.SetParent(transform);
        spawnedBlock = true;
    }

}
