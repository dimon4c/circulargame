﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TblockLine : TCellDescription
{

    public override Indexes[] BlockIndexes
    {

        get
        {
            if (blockIndexes == null)
                blockIndexes = new []{ new Indexes(1, 0), new Indexes(0, 0) };
               
            return blockIndexes;
            //            { new Indexes(1, 0), new Indexes(-1, 0), new Indexes(1, 1) };
        }
    }

    public override void Start()
    {
        cellSpriteType = 5;
        this.gameObject.transform.GetChild(0).GetComponent<TDragCtrl>().SetParentScript(this);
        cellSprite = new List<Sprite>();
        for (int i = 0; i < blockSize; i++)
            cellSprite.Add(HomeCtrl.GetInstanse().BBSprites[cellSpriteType, i]);
        CreateMap();
        mapHolder = GameObject.Find("Tutorial");
        tScript = mapHolder.GetComponent<Tutorial>();
        defaultRotation = cellHolder.transform.rotation;
        startLocalPos = transform.localPosition;
        topDistance = tScript.GetMapDistance();

    }
}
