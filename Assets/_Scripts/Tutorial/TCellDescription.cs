﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TCellDescription : MonoBehaviour
{
  
   


    protected const int blockSize = 4;
    protected const int blockCount = 4;
    float startY = 0.3f;
    const float cellSpaceY = 0.15f;
    const float rotationStep = 45f;
    GameObject[,] gameCells = new GameObject[blockCount, blockSize];

    protected Vector3 startPos;
    protected Vector3 startLocalPos;
    public GameObject cellHolder;
    public List<Sprite> cellSprite;
    public Indexes[] blockIndexes;
    //    bool enabled = true;
    /*
    protected float horizontalStep = 5.5f;
    protected float verticalStep = 3f;
    */
    protected float scaleModifier = 0.3f;
    public int blockIndex;


    public virtual Indexes[] BlockIndexes
    {
        set
        {
            blockIndexes = new Indexes[]{ };
        }
        get
        {
            return blockIndexes;
        }
    }

    public Tutorial tScript;
    protected Color blockColor = Color.white;
    public int cellSpriteType;

    protected  CellInfo collidedCell;
    protected bool collided = false;

    public GameObject mapHolder;
    protected float maxRadius = 0.3f;


	public float topDistance;
    protected Quaternion defaultRotation;

    int GetMinBlock()
    {
        int minBlockIndex = 0;
        foreach (Indexes i in BlockIndexes)
        {
            if (i.byBlock < minBlockIndex)
                minBlockIndex = i.byBlock;
        }
        return minBlockIndex;
    }

    int GetMinCell()
    {
        int minCellIndex = 0;
        foreach (Indexes i in BlockIndexes)
        {
            if (i.byPos < minCellIndex)
                minCellIndex = i.byPos;
        }
        return minCellIndex;
    }

    protected int GetBlockWidth()
    {
        List<int> blockWidth = new List<int>();
        foreach (Indexes i in BlockIndexes)
        {
            if (!blockWidth.Contains(i.byBlock))
                blockWidth.Add(i.byBlock);
        }
        return blockWidth.Count;
    }

    protected int GetBlockHeight()
    {
        List<int> blockHeight = new List<int>();
        foreach (Indexes i in BlockIndexes)
        {
            if (!blockHeight.Contains(i.byPos))
                blockHeight.Add(i.byPos);
        }
        return blockHeight.Count;
    }

    protected void CreateMap()
    {
        List<Indexes> ind = new List<Indexes>(BlockIndexes);
        //        int minBlockIndex = GetMinBlock();
        //        int minCellIndex = GetMinCell();
        for (int i = 0; i < blockCount; i++)
        {
            for (int k = 0; k < blockSize; k++)
            {
                Indexes ci;
                ci.byBlock = i - 1;
                ci.byPos = k - 1;
                if (ind.Contains(ci))
                {

                    GameObject cell = new GameObject("CellImage_" + k);
                    SpriteRenderer renderer = cell.AddComponent<SpriteRenderer>();
                    renderer.sprite = cellSprite[k];
                    renderer.flipY = true;
                    float x = cellHolder.transform.position.x;
                    float height = cellHolder.transform.position.y + startY + cellSpaceY * k;
                    cell.transform.position = new Vector3(x, height);
                    cell.transform.localScale = new Vector3(cell.transform.localScale.x * scaleModifier, cell.transform.localScale.y * scaleModifier);
                    cell.transform.SetParent(cellHolder.transform);



                    gameCells[i, k] = cell;
                }
            }
            cellHolder.transform.Rotate(0f, 0f, rotationStep);
        }

        //        cellHolder.transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
        float angle = rotationStep * blockCount / 2 + -rotationStep / 2;

        cellHolder.transform.Rotate(0f, 0f, angle);

        if (GetBlockWidth() == 1)
        {
            cellHolder.transform.Rotate(0f, 0f, -rotationStep / 2);
        }
//        cellHolder.transform.position = cellHolder.transform.position + new Vector3(0f, 0.15f);
    }


   


    public virtual void Start()
    {
        cellSpriteType = UnityEngine.Random.Range(1, HomeCtrl.spriteTypesCount);
        this.gameObject.transform.GetChild(0).GetComponent<TDragCtrl>().SetParentScript(this);
        cellSprite = new List<Sprite>();
        for (int i = 0; i < blockSize; i++)
            cellSprite.Add(HomeCtrl.GetInstanse().BBSprites[cellSpriteType, i]);
        CreateMap();
        mapHolder = GameObject.Find("Tutorial");
        tScript = mapHolder.GetComponent<Tutorial>();
        defaultRotation = cellHolder.transform.rotation;
        startLocalPos = transform.localPosition;
        topDistance = tScript.GetMapDistance();

    }

    public virtual void OnRemove()
    {
      
        Destroy(gameObject);

    }

    public void RotateBlock()
    {
        if (mapHolder == null)
        {
            mapHolder = GameObject.Find("Tutorial");
            return;
        }
   
        if (Vector3.Distance(cellHolder.transform.position, mapHolder.transform.position) <= maxRadius || Vector3.Distance(cellHolder.transform.position, mapHolder.transform.position) > topDistance)
        {
            cellHolder.SetActive(true);
        }
        if (tScript.currentHighlight.Count < 0)
        {
            cellHolder.SetActive(true);
        }
        Vector3 targetDir = mapHolder.transform.position - transform.position;


        float angle = Mathf.Atan2(targetDir.x, targetDir.y) * Mathf.Rad2Deg;

        Vector3 targetDir2 = mapHolder.transform.position - transform.position;


        transform.rotation = Quaternion.Euler(0.0f, 0.0f, -angle);


        //*/
    }




    public void OnMouseUp2(Vector3 startPos)
    {
        if (collidedCell == null || !tScript.SetBlock(BlockIndexes, cellSpriteType, collidedCell.blockId, collidedCell.cellId, blockColor))
        {
            transform.position = startPos;
            cellHolder.transform.rotation = defaultRotation;
            cellHolder.SetActive(true);
        }
        else
        {

           
            OnRemove();
        }
    }

    void OnTriggerEnter(Collider other)
    {

        cellHolder.SetActive(false);
        //            collided = true;
        collidedCell = other.gameObject.GetComponent<CellInfo>();

        tScript.CheckBlock(BlockIndexes, cellSpriteType, collidedCell.blockId, collidedCell.cellId, blockColor);

        //*/
    }


    //*/
    void OnTriggerExit(Collider other)
    {

        if (other.gameObject.GetComponent<CellInfo>() == collidedCell)
        {
            collided = false;
            collidedCell = null;
            tScript.RestoreCellColors();
        }
        // Restore status
    }
    //*/


}
