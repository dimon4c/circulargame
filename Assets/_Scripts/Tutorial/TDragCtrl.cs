﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TDragCtrl : MonoBehaviour
{

    protected Vector3 startPos;
    public TCellDescription scr;


    void Start()
    {
        SphereCollider c = gameObject.AddComponent<SphereCollider>();
        c.radius = 0.75f;
        Destroy(gameObject.GetComponent<BoxCollider>());
    }

    public void OnMouseDown()
    {
        startPos = transform.parent.position;
    }

    public void OnMouseDrag()
    {
        if (PowerUpInfo.onPause)
            return;

		Vector3 newPos = Camera.main.ScreenToWorldPoint(Input.mousePosition) + Vector3.up * 0.66f;
		if (scr.topDistance > 0)
		{
			newPos += Vector3.up * (Vector3.Distance(startPos, transform.parent.position) / Vector3.Distance(scr.mapHolder.transform.position, startPos));
		}

        newPos.z = transform.parent.position.z;
        transform.parent.position = newPos;
        scr.RotateBlock();
    }


    public void SetParentScript(TCellDescription parent)
    {
        scr = parent;
    }

    public void OnMouseUp()
    {

        scr.OnMouseUp2(startPos);

    }
}
