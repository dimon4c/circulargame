﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//using UnityEditor.MemoryProfiler;
using System.IO;
using System;
using Microsoft.Win32;
using GoogleMobileAds.Api;



public class HomeCtrl : MonoBehaviour
{
    public Sprite[] clearSprites;
    public static int spriteTypesCount = 9;
    //---------------------------------------------------------
    public GameObject homescreen, powerScreen;
    public GameObject mapPrefab, mapHolder;
    public List<GameObject> gameScreen;
    public Text currentScore, hiScore, money, money2, money3, money4;
    public GameObject gameOverHolder, tutorialPrefab, video2xHelpPanel, video2xHelpBtn;

    public GameObject BBParent;
    public Sprite[,] BBSprites = new Sprite[spriteTypesCount, 8];
    //---------------------------------------------------------
    //added
    public GameObject x2image, videobtnx2, videobtnRevive, powerupsWatchAdBtn, storeScreen, titleDay, titleNight, gameDay, gameNight, pauseDay, pauseNight, powerDay, powerNight, mutedBtn, unmutedBtn, mutedBtnPause, unmutedBtnPause, gameoverDay, gameoverNight;
    public GameObject storeRemoveAdsBtn, store2xButton;
    public GameObject demoStep1, demoStep2, demoCaption1, demoCaption2;
    public GameObject[] powerUpButtons = new GameObject[6];
    int IsNightMode = 0;
    public GameServices GameServices;
    float startTime = 0;
    float totalPlayTime = 0;
    int gamesPlayed = 0;
    public bool revived = false;
    public bool wasGameOver = false;
    public bool DoubleCoinsForOneGame;
    GameObject tutorial;
    public GameObject helper;
    public GameObject scoreHolder;
    public GameObject scoreHolderCrystals;
    public GameObject scoreHolderBest;
    public GameObject scoreHolderCurrent;
    public GameObject crystalAnim, crystalAnimText;
    public GameObject puAddCrystalAnim, puAddCrystalAnimText;
    public SaveForma savedData;
    public bool rewVideoWatched = false;
    public bool GiveTutorialMoney = false;

    //...
    //---------------------------------------------------------
    public delegate void PlayEvents();
    //---------------------------------------------------------
    public event PlayEvents OnPlay;
    //---------------------------------------------------------
    public void PlayGame()
    {
		
        if (PlayerPrefs.GetInt("2xcoins", 0) == 0)
        {
            x2image.SetActive(false);
            DoubleCoinsForOneGame = false;
        }
        else
        {
            x2image.SetActive(true);
            DoubleCoinsForOneGame = true;
        }

        if (PlayerPrefs.GetInt("2xhelp", 0) == 0)
        {
            video2xHelpBtn.SetActive(true);
        }
        else
        {
            video2xHelpBtn.SetActive(false);
        }

        if (OnPlay != null)
        {
            OnPlay();
        }
    }

    //---------------------------------------------------------
    private static HomeCtrl instanse;
    //---------------------------------------------------------
    public static HomeCtrl GetInstanse()
    {
        return instanse;
    }
    //---------------------------------------------------------
    void Awake()
    {
        if (instanse == null)
        {
            instanse = this;
        }
//        PlayerPrefs.SetInt("Money", 1000);
//        DeleteSave();
        //test
//        PlayerPrefs.DeleteAll();

        //PlayerPrefs.SetInt("Tutorial", 0);
//        PlayerPrefs.SetInt("Money", 1000);
        

       
    }
    //---------------------------------------------------------


    void CheckSave()
    {
        string game = PlayerPrefs.GetString("GameState", "");
        if (!string.IsNullOrEmpty(game))
        {
            savedData = new SaveForma();
            JsonUtility.FromJsonOverwrite(game, savedData);
//            PlayerPrefs.SetInt("Save", 1);
            PreparePowers(savedData);
            homescreen.SetActive(false);
            PowerUpsManager.Instance.Reset();
            PowerUpsManager.Instance.ShowSaved();
            PlayGame();
        }
//        StartCoroutine(LoadSave());
    }

    void PreparePowers(SaveForma loaded)
    {
        PowerUpInfo.activated = new List<PowerUps>();
        for (int i = 0; i < loaded.savedPowerUps.Count; i++)
        {
            PowerUpInfo.activated.Add((PowerUps)loaded.savedPowerUps[i]);
        }
//        PowerUpsManager.Instance.ShowSaved();
//        PowerUpActivator.GetInstanse().CheckPowerUps();
    }

    IEnumerator LoadSave()
    {
        string path = Application.persistentDataPath + "/Save.json";
        Debug.Log(path);
        if (File.Exists(path))
        {
            WWW www = new WWW(path);
            yield return www;
            if (!string.IsNullOrEmpty(www.error))
                Debug.Log(www.error);
            Debug.Log("Check complete");
            Debug.Log(www.text);
            string json = www.text;
//            Debug.Log(json);
            Debug.Log(www.bytesDownloaded);
//            savedData = JsonUtility.FromJson<SaveForma>(json);
            savedData = new SaveForma();
            JsonUtility.FromJsonOverwrite(json, savedData);
            PlayerPrefs.SetInt("Save", 1);
        }
        else
            PlayerPrefs.SetInt("Save", 0);
    }

    List<BlockSaveData> GetBlocks()
    {
        List<BlockSaveData> savedBlocks = new List<BlockSaveData>();
        CellDescription[] currentBlocks = BlockGen.GetInstanse().GetCurrentBlocks();
        foreach (CellDescription c in currentBlocks)
        {
            BlockSaveData b = new BlockSaveData();
            b.blockId = c.blockIndex;
            b.blockSpryteType = c.cellSpriteType;
            b.blockX = c.gameObject.transform.localPosition.x;
            b.blockY = c.gameObject.transform.localPosition.y;
            b.blockZ = c.gameObject.transform.localPosition.z;
            savedBlocks.Add(b);
        }
        return savedBlocks;
    }

    List<CellSaveData> GetCells()
    {
        List<CellSaveData> savedCells = new List<CellSaveData>();
        List<CellInfo> usedCells = MapGen.GetInstanse().GetUsedCells();
        foreach (CellInfo c in usedCells)
        {
            CellSaveData cell = new CellSaveData();
            cell.cellBlock = c.blockId;
            cell.cellPos = c.cellId;
            cell.cellSpriteType = c.spriteType;
            savedCells.Add(cell);

        }
        return savedCells;
    }

    List<int> GetPowerUps()
    {
        List<int> savedPowerUps = new List<int>();
        foreach (PowerUps p in PowerUpInfo.activated)
        {
            savedPowerUps.Add((int)p);
        }
        return savedPowerUps;
    }

    public void DeleteSave()
    {
        PlayerPrefs.SetString("GameState", "");
        /*/
        PlayerPrefs.SetInt("Save", 0);
        string path = Application.persistentDataPath + "/Save.json";
        Debug.Log(path);
        if (File.Exists(path))
            File.Delete(path);
        //*/
    }

    public SaveForma PrepareSave()
    {
        SaveForma newSave = new SaveForma();
        newSave.score = PowerUpInfo.playerCurrentScore;
        newSave.currentBlocks = GetBlocks();
        newSave.usedCells = GetCells();
        newSave.savedPowerUps = GetPowerUps();
        return newSave;
    }

    public void CreateSave(SaveForma newSave)
    {
        /*/
        string path = Application.persistentDataPath + "/Save.json";
        if (File.Exists(path))
            File.Delete(path);
      
        Debug.Log(json);
        File.WriteAllText(path, json);
        //*/
        string json = JsonUtility.ToJson(newSave);
        PlayerPrefs.SetString("GameState", json);
//        PlayerPrefs.SetInt("Save", 1);
    }

    public void CallCreateSave()
    {
        CreateSave(PrepareSave());
    }

    void OnApplicationQuit()
    {
        if (MapGen.GetInstanse() != null)
        {
            if ((PlayerPrefs.GetInt("Tutorial", 0) != 0 && !PauseMgr.GetInstanse().tutorialFinished))
                CallCreateSave();
        }
    }

    void Start()
    {
        
        PowerUpInfo.playerStars = PlayerPrefs.GetInt("Money", 0);
        if (PlayerPrefs.GetInt("2xcoins", 0) == 1)
        {
            x2image.SetActive(true);
            videobtnx2.SetActive(false);
        }
        else
        {
            x2image.SetActive(false);
            //videobtnx2.SetActive (true); will appear after video was loaded
        }

        UpdateMoney(PowerUpInfo.playerStars);

        LoadScore();
        OnPlay += ResetCurrentScore;
        OnPlay += DisplayGameScreen;
        PauseMgr.GetInstanse().OnHome += DisplayHomeScreen;
        PauseMgr.GetInstanse().OnHome += UpdateScoreHolder;

        for (int i = 0; i < spriteTypesCount; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                BBSprites[i, j] = BBParent.transform.GetChild(i).GetChild(j).gameObject.GetComponent<SpriteRenderer>().sprite;
            }
        }
        //added...
        IsNightMode = PlayerPrefs.GetInt("nightmode", 0);
        titleDay.SetActive(IsNightMode == 0);
        titleNight.SetActive(IsNightMode != 0);
        gameDay.SetActive(IsNightMode == 0);
        gameNight.SetActive(IsNightMode != 0);
        pauseDay.SetActive(IsNightMode == 0);
        pauseNight.SetActive(IsNightMode != 0);
        powerDay.SetActive(IsNightMode == 0);
        powerNight.SetActive(IsNightMode != 0);
        gameoverDay.SetActive(IsNightMode == 0);
        gameoverNight.SetActive(IsNightMode != 0);

        if (PlayerPrefs.GetInt("muted", 0) == 0)
        {
            mutedBtn.SetActive(false);
            mutedBtnPause.SetActive(false);
            unmutedBtn.SetActive(true);
            unmutedBtnPause.SetActive(true);
        }
        else
        {
            mutedBtn.SetActive(true);
            mutedBtnPause.SetActive(true);
            unmutedBtn.SetActive(false);
            unmutedBtnPause.SetActive(false);
        }
        //...

        Input.multiTouchEnabled = false;
        UpdateScoreHolder();

        if (PlayerPrefs.GetInt("Tutorial", 0) == 0 || PauseMgr.GetInstanse().tutorialFinished)
        {
            x2image.SetActive(false);
            powerupsWatchAdBtn.SetActive(false);
            if (PlayerPrefs.GetInt("Tutorial", 0) == 0)
                StartTutorial();
        }
        else
        {
            CallCheckSave();
        }

        if (GameServices == null)
        {
            GameServices = new GameServices();
        }
        
    }

    void CallCheckSave()
    {
        StartCoroutine(ICheckSave());
    }

    IEnumerator ICheckSave()
    {
        yield return new WaitForSeconds(0.05f);
        CheckSave();
    }
    //---------------------------------------------------------
    void OnDisable()
    {
        OnPlay -= ResetCurrentScore;
        OnPlay -= DisplayGameScreen;
        PauseMgr.GetInstanse().OnHome -= DisplayHomeScreen;
        PauseMgr.GetInstanse().OnHome -= UpdateScoreHolder;
    }

    //---------------------------------------------------------
   

    void DisplayHomeScreen()
    {
        rewVideoWatched = false;
        CheckSave();
        scoreHolder.SetActive(false);
        scoreHolderCrystals.SetActive(true);
        scoreHolderBest.SetActive(true);
        scoreHolderCurrent.SetActive(true);

        CheckScore();
        homescreen.SetActive(true);
        gameOverHolder.SetActive(false);
        foreach (GameObject o in gameScreen)
        {
            o.SetActive(false);
        }
        PowerUpInfo.gameOver = false;

        if (!Admob.Instance.IsRewardedVideoLoaded())
        {
            Admob.Instance.LoadVideo();
        }
    }
    //---------------------------------------------------------

    public void TutorialHomeFinish()
    {
        Invoke("ShowPowerUpsAfterTutorialAfterDelay", 1.5f);
    }

    void DisplayGameOverAfterDelay()
    {
        gameOverHolder.SetActive(true);
      
        bool shown = RateDialog();

        if (!shown)
        {
            Admob.Instance.ShowStaticInterstitial();
        }

    }
    //---------------------------------------------------------
    void ShowPowerUpsAfterTutorialAfterDelay()
    {
        PauseMgr.GetInstanse().Home();

//        ShowPowerUp();
    }
    //---------------------------------------------------------
    void DisplayHomeAfterDelay()
    {
        PauseMgr.GetInstanse().Home();
        //Admob.Instance.ShowInterstitial();
    }
    //---------------------------------------------------------
    void DisplayGameOver()
    {
        DeleteSave();
        /*
        if (PowerUpInfo.playerHiScore == PowerUpInfo.playerCurrentScore)
        {
            GameServices.ReportScore(PowerUpInfo.playerCurrentScore);
        }
        */
        PlayerPrefs.SetInt("Money", PowerUpInfo.playerStars);

        if (!revived && !PauseMgr.GetInstanse().tutorialFinished)
        {
            Invoke("DisplayGameOverAfterDelay", 1.5f);
            wasGameOver = true;
        }
        else
        {
            if (PauseMgr.GetInstanse().tutorialFinished)
            {
                PauseMgr.GetInstanse().tutorialFinished = false;
                Invoke("ShowPowerUpsAfterTutorialAfterDelay", 1.5f);

            }
            else
            {
                Invoke("DisplayHomeAfterDelay", 1.5f);
            }
        }
    }

    bool dialogQuitVisible = false;
    float lastOpenedTime = 0;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (homescreen.activeSelf)
            { 
                if (!dialogQuitVisible)
                {
                    if (lastOpenedTime == 0 || (Time.time - lastOpenedTime > 1f))
                    {
                        //Debug.Log ("test");

                        dialogQuitVisible = true;
                        lastOpenedTime = Time.time;
                        MobileNativeDialog dialogQuit = new MobileNativeDialog("Quit", "Exit the game?");
                        dialogQuit.OnComplete += OnDialogClose;
                    }
                }
            }
            if (MapGen.GetInstanse() != null && !gameOverHolder.activeSelf)
            {
                if (!PauseMgr.GetInstanse().pauseHolder.activeSelf)
                {
                    PauseMgr.GetInstanse().StartPause();
                }
                else
                {
                    PauseMgr.GetInstanse().Resume();
                }
            }
            if (powerScreen.activeSelf)
            {
                powerScreen.SetActive(false);
                homescreen.SetActive(true);
            }
        }
    }

    private void OnDialogClose(MNDialogResult result)
    {
        //parsing result
        dialogQuitVisible = false;
        switch (result)
        {
            case MNDialogResult.YES:
                Application.Quit();
                break;
            case MNDialogResult.NO:
                break;
        }
    }

    //---------------------------------------------------------
    bool dialogRateVisible = false;

    bool RateDialog()
    {
        if (dialogRateVisible)
        {
            return false;
        }

        dialogRateVisible = true;

        float endTime = Time.time;
        float levelDur = endTime - startTime;
        totalPlayTime += levelDur;
        PlayerPrefs.SetFloat("playtime", totalPlayTime);
        gamesPlayed++;
        PlayerPrefs.SetInt("games", gamesPlayed);
        bool shown = false;
        if (gamesPlayed >= 2)
        {
            if (PlayerPrefs.GetInt("rate", 0) == 0 && totalPlayTime >= 10 * 60)
            {
                MobileNativeRateUs ratePopUp = new MobileNativeRateUs("Do you like this Game?", "Please take a moment to rate it. Thanks for your support!", "RATE", "LATER", "NO, THANKS");
                ratePopUp.SetAndroidAppUrl("https://play.google.com/store/apps/details?id=com.circular.puzzle");
                ratePopUp.OnComplete += OnRatePopUpClose;
                ratePopUp.Start();
                shown = true;
            }
            if (PlayerPrefs.GetInt("rate", 0) == 1 && totalPlayTime >= 50 * 60)
            {
                PlayerPrefs.SetInt("rate", 0);
                PlayerPrefs.SetFloat("playtime", 0);
            }
        }
        return shown;
    }

    //---------------------------------------------------------
    void HideGameOver()
    {
        gameOverHolder.SetActive(false);
    }
    //---------------------------------------------------------
    void StartTutorial()
    {
        homescreen.SetActive(false);
        powerScreen.SetActive(false);

        scoreHolderCrystals.SetActive(false);
        scoreHolderBest.SetActive(false);
        scoreHolderCurrent.SetActive(false);

        tutorial = Instantiate(tutorialPrefab);
        tutorial.name = "Tutorial";
    }

    void DisplayGameScreen()
    {
        scoreHolder.SetActive(true);
        if (PauseMgr.GetInstanse().tutorialFinished)
        {
            //scoreHolder.SetActive(true);
            scoreHolderCurrent.SetActive(true);
            scoreHolderCrystals.SetActive(false);
            scoreHolderBest.SetActive(false);
            /*
			for (int i = 0; i < scoreHolder.transform.childCount; i++) {
				scoreHolder.transform.GetChild (i).gameObject.SetActive (false);
			}
			scoreHolder.transform.GetChild (2).gameObject.SetActive (true);
			*/
        }
        else
        {
            scoreHolderCrystals.SetActive(true);
            scoreHolderCrystals.GetComponent<Animator>().enabled = false;

            scoreHolderBest.SetActive(true);
            scoreHolderCurrent.SetActive(true);
        }
        homescreen.SetActive(false);
        powerScreen.SetActive(false);
        GameObject go = Instantiate(mapPrefab);
        go.name = "MapHolder";
        go.GetComponent<MapGen>().OnGameOver += DisplayGameOver;
        go.transform.position = mapHolder.transform.position;
        go.SetActive(true);
        foreach (GameObject o in gameScreen)
        {
            o.SetActive(true);
        }
        startTime = Time.time;
        gamesPlayed = PlayerPrefs.GetInt("games", 0);
        wasGameOver = false;
        PowerUpInfo.onPause = false;
        if (tutorial != null)
        {
            DestroyImmediate(tutorial);
            demoStep1.SetActive(false);
            demoCaption1.SetActive(false);
            demoStep2.SetActive(false);
            demoCaption2.SetActive(false);
        }
        if (PauseMgr.GetInstanse().tutorialFinished || PlayerPrefs.GetInt("Tutorial", 0) == 0)
        {
            x2image.SetActive(false);
            videobtnx2.SetActive(false);
        }
        else
        {
            if (PlayerPrefs.GetInt("2xcoins", 0) == 1)
            {
                x2image.SetActive(true);
                videobtnx2.SetActive(false);
            }
            else
            {
                x2image.SetActive(false);
                //videobtnx2.SetActive (true); will appear after video was loaded
            }
        }
//        BlockGen.GetInstanse().SwapBlocks();
    }
    //---------------------------------------------------------
    void LoadScore()
    {
        PowerUpInfo.LoadScore();
    }
    //---------------------------------------------------------
    void CheckScore()
    {
        PowerUpInfo.CheckScore();
    }

    //---------------------------------------------------------
    void ResetCurrentScore()
    {
        revived = false;
       
        PowerUpInfo.ResetCurrentScore();
        ScoreCtrl.GetInstanse().UpdateScore();
        ScoreCtrl.GetInstanse().UpdateHiScore();
    }
    //---------------------------------------------------------
    public IEnumerator UpdateCrystalsAfterDelay(int ammount)
    {
        scoreHolderCrystals.GetComponent<Animator>().enabled = false;

        crystalAnim.SetActive(true);
        crystalAnim.GetComponent<Animator>().enabled = true;
        crystalAnimText.GetComponent<TextMesh>().text = " + " + ammount;

        yield return new WaitForSeconds(1f);

        scoreHolderCrystals.SetActive(false);
        scoreHolderCrystals.SetActive(true);
        scoreHolderCrystals.GetComponent<Animator>().enabled = true;
        //scoreHolderCrystals.GetComponent<Animator> ().Play ("CrystalsInGame");

        crystalAnim.GetComponent<Animator>().enabled = false;
        crystalAnim.SetActive(false);

        UpdateMoney(PowerUpInfo.playerStars);
    }
    //---------------------------------------------------------
    public void UpdateMoney(int ammount)
    {
        //Debug.Log ("amount " + ammount);
        money.text = ammount.ToString();
        money2.text = ammount.ToString();
        money3.text = ammount.ToString();
        money4.text = ammount.ToString();
    }
    //---------------------------------------------------------

    public void AddScore(int score)
    {
        //if (!PauseMgr.GetInstanse().tutorialFinished)
        StartCoroutine(IAddScore(score));
    }

    IEnumerator IAddScore(int score)
    {
        int step = (int)score / 3;
        int temp = score - step * 3;
        for (int i = 0; i < score - temp; i += step)
        {
            PowerUpInfo.playerCurrentScore += step;
            UpdateScoreHolder();
            ScoreCtrl.GetInstanse().UpdateScore();
//            PowerUpInfo.CheckScore();
//            ScoreCtrl.GetInstanse().UpdateHiScore();
            yield return new WaitForSeconds(0.10f);
        } 
        PowerUpInfo.playerCurrentScore += temp;
        UpdateScoreHolder();
        ScoreCtrl.GetInstanse().UpdateScore();
//        Debug.Log(PowerUpInfo.playerCurrentScore);
        yield return new WaitForSeconds(0.10f);
    }

    public void UpdateScoreHolder()
    {
        currentScore.text = PowerUpInfo.playerCurrentScore.ToString();
        hiScore.text = PowerUpInfo.playerHiScore.ToString();
        //money.text = PowerUpInfo.playerStars.ToString();
        //UpdateMoney(PowerUpInfo.playerStars);

    }
    //---------------------------------------------------------
    public void SwitchTheme()
    {
        //added...
        if (IsNightMode == 0)
        {
            IsNightMode = 1;
        }
        else
        {
            IsNightMode = 0;
        }

        titleDay.SetActive(IsNightMode == 0);
        titleNight.SetActive(IsNightMode != 0);
        gameDay.SetActive(IsNightMode == 0);
        gameNight.SetActive(IsNightMode != 0);
        pauseDay.SetActive(IsNightMode == 0);
        pauseNight.SetActive(IsNightMode != 0);
        powerDay.SetActive(IsNightMode == 0);
        powerNight.SetActive(IsNightMode != 0);
        gameoverDay.SetActive(IsNightMode == 0);
        gameoverNight.SetActive(IsNightMode != 0);

        PlayerPrefs.SetInt("nightmode", IsNightMode);
        //...
    }
    //---------------------------------------------------------
    public void OpenStore()
    {
        if (PlayerPrefs.GetInt("noads", 0) == 1)
        {
            storeRemoveAdsBtn.SetActive(false);
        }
        if (PlayerPrefs.GetInt("2xcoins", 0) == 1)
        {
            store2xButton.SetActive(false);
        }
        storeScreen.SetActive(true);
    }
    //---------------------------------------------------------
    private void OnRatePopUpClose(MNDialogResult result)
    {
        dialogRateVisible = false;
        //parsing result
        switch (result)
        {
            case MNDialogResult.RATED:
                PlayerPrefs.SetInt("rate", 2);
                break;
            case MNDialogResult.REMIND:
                PlayerPrefs.SetInt("rate", 1);
                break;
            case MNDialogResult.DECLINED:
                PlayerPrefs.SetInt("rate", 2);
                break;
        }
    }
    //---------------------------------------------------------
    public void OpenLeaderboard()
    {
        GameServices.ShowLeaderboard();
    }
    //---------------------------------------------------------
    public void ShowPowerUp()
    {
        if (GiveTutorialMoney)
        {
            AddMoneyRolling(50);
            GiveTutorialMoney = false;
        }
        if (PlayerPrefs.GetInt("Tutorial", 0) == 0 || PauseMgr.GetInstanse().tutorialFinished)
        {
//            || PauseMgr.GetInstanse().tutorialFinished
            x2image.SetActive(false);
            powerupsWatchAdBtn.SetActive(false);
            if (PlayerPrefs.GetInt("Tutorial", 0) == 0)
                StartTutorial();
        }
        else
        {
            /*/
            if (!string.IsNullOrEmpty(PlayerPrefs.GetString("GameState", "")))
            {
                PlayGame();
            }
            else 
                //*/
            if (RewardBasedVideoAd.Instance.IsLoaded())
            {
               
                HomeCtrl.GetInstanse().powerupsWatchAdBtn.SetActive(true);

            }
           
            if (PlayerPrefs.GetInt("2xcoins", 0) == 1)
            {
                x2image.SetActive(true);
                videobtnx2.SetActive(false);
            }
            else
            {
                x2image.SetActive(false);
                //videobtnx2.SetActive (true); will appear after video was loaded
            }

            homescreen.SetActive(false);

            //added
            PowerUpsManager.Instance.Reset();
            PowerUpsManager.Instance.ShowSaved();
            for (int i = 0; i < 6; i++)
            {
                powerUpButtons[i].GetComponent<Animator>().enabled = false;
            }

            powerScreen.SetActive(true);
           
        }
    }
    //---------------------------------------------------------
    public void PowerupFlip(int id)
    {
        powerUpButtons[id].GetComponent<Animator>().enabled = true;
        powerUpButtons[id].GetComponent<Animator>().Play("PowerUpFlip");
    }
    //---------------------------------------------------------
    public void PowerupFlipBack(int id)
    {
        powerUpButtons[id].GetComponent<Animator>().Play("PowerUpFlipBack");
    }
    //---------------------------------------------------------
    public void OnAdsRemoved()
    {
        PlayerPrefs.SetInt("noads", 1);
        Admob.Instance.RemoveBanner();
        storeRemoveAdsBtn.SetActive(false);
    }
    //---------------------------------------------------------
    public void OnDoubleCoinsPurchased()
    {
        PlayerPrefs.SetInt("2xcoins", 1);
        videobtnx2.SetActive(false);
        store2xButton.SetActive(false);
    }
    //---------------------------------------------------------
    public void OnCoinsPurchased(int count)
    {
        PowerUpInfo.playerStars += count;
        PlayerPrefs.SetInt("Money", PowerUpInfo.playerStars);
        UpdateMoney(PowerUpInfo.playerStars);
    }
    //---------------------------------------------------------
    public void UIGameOverHome()
    {
        gameOverHolder.SetActive(false);
        homescreen.SetActive(true);
    }
    //---------------------------------------------------------
    public void UIGameOverRestart()
    {
        gameOverHolder.SetActive(false);
        PlayGame();
    }
    //---------------------------------------------------------
    public void UIGameOverRevive500()
    {
        if (!revived && PowerUpInfo.playerStars >= 100)
        {
            
            revived = true;
            gameOverHolder.SetActive(false);
            MapGen.GetInstanse().UndoMove();
            PowerUpInfo.RemoveMoney(100);
            UpdateMoney(PowerUpInfo.playerStars);
            
        }

    }
    //---------------------------------------------------------
    public void UIGameOverReviveVideo()
    {
        Admob.Instance.ShowRewardBasedVideo(2); //revive
    }
    //---------------------------------------------------------

    public void AddMoneyRolling(int score)
    {
        if (PauseMgr.GetInstanse().tutorialFinished)
            return;

        puAddCrystalAnim.SetActive(false);
        puAddCrystalAnim.SetActive(true);
        puAddCrystalAnim.GetComponent<Animator>().enabled = true;
        puAddCrystalAnimText.GetComponent<Text>().text = " + " + score;

        StartCoroutine(IAddMoney(score));
    }
    /*
	public void AddMoneyNoGUIUpdate(int score)
	{
		if (PauseMgr.GetInstanse().tutorialFinished)
			return;
		PowerUpInfo.AddMoney(score);
	}
*/
    IEnumerator IAddMoney(int score)
    {
        int step = (int)score / 20;
        int temp = score - step * 20;
        for (int i = 0; i < score - temp; i += step)
        {
            PowerUpInfo.AddMoney(step);
            HomeCtrl.GetInstanse().UpdateMoney(PowerUpInfo.playerStars);
            yield return new WaitForSeconds(0.1f);
        } 
        PowerUpInfo.AddMoney(temp);
        HomeCtrl.GetInstanse().UpdateMoney(PowerUpInfo.playerStars);
        yield return new WaitForSeconds(0.1f);

        puAddCrystalAnim.SetActive(false);
    }
    //---------------------------------------------------------
    public void UI2xCoinsVideo()
    {
        if (PlayerPrefs.GetInt("2xhelp", 0) == 1)
        {
            Admob.Instance.ShowRewardBasedVideo(3); //2x
        }
        else
        {
            ShowVideo2xHelp();
        }
    }
    //---------------------------------------------------------
    public void UIVideoForCrystals()
    {
        rewVideoWatched = true;
        Admob.Instance.ShowRewardBasedVideo(1); //crystals
    }
    //---------------------------------------------------------
    public void OnRewardedVideo1Watched()
    {
        //add crystals to power ups screen
        AddMoneyRolling(20);
    }
    //---------------------------------------------------------
    public void OnRewardedVideo2Watched()
    {
        //revive, do not deduct 500 crystals
        if (!revived)
        {
            revived = true;
            gameOverHolder.SetActive(false);
            MapGen.GetInstanse().UndoMove();
        }
    }
    //---------------------------------------------------------
    public void OnRewardedVideo3Watched()
    {
        //show 2x crystals sprite for one game, set a variable that 2x mode is in progress
        //and x2 crystals when added
        DoubleCoinsForOneGame = true;
        x2image.SetActive(true);
    }
    //---------------------------------------------------------
    public void ShowVideo2xHelp()
    {
        PlayerPrefs.SetInt("2xhelp", 1);
        video2xHelpPanel.SetActive(true);
        video2xHelpBtn.SetActive(false);
    }
    //---------------------------------------------------------

    public void StartMove(GameObject obj, Vector3 fromPos, Vector3 toPos)
    {
        StartCoroutine(IMoveObject(obj, fromPos, toPos));
    }

    IEnumerator IMoveObject(GameObject obj, Vector3 fromPos, Vector3 toPos)
    {
        GameObject go = obj;
        go.transform.position = fromPos;
        float step = 25f * Time.deltaTime;
        Vector3 previousPos = go.transform.position;
//        Debug.Log(direction);
        while (Vector3.Distance(go.transform.position, toPos) > 0.1f)
        {
            Debug.Log((go.transform.position - toPos).magnitude);
            go.transform.position = Vector3.MoveTowards(go.transform.position, toPos, step);
            yield return new WaitForSeconds(0.06f);
            if (Vector3.Distance(go.transform.position, toPos) < Vector3.Distance(previousPos, toPos))
            {
                previousPos = go.transform.position;
            }
            else
                break;

        }
        Destroy(go);
    }
}
