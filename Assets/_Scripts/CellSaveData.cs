﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class CellSaveData
{
    public int cellBlock;
    public int cellPos;
    public int cellSpriteType;

   
}
