﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames.BasicApi;

public class MapGen : MonoBehaviour
{
    const int blockSize = 8;
    const int blockCount = 8;

    float startY = 0.55f;
    const float cellSpaceY = 0.306f;
    const float rotationStep = 45f;

    [Header("CellPrefabs")]
    [Tooltip("Prefabs have to be set in ascending order starting from the smallest")]
    public GameObject[] cellPrefabs = new GameObject[blockSize];
    public List<CellInfo> currentHighlight, setCells;
    public int currentBlockId;
    public Vector3 blockPos;
   
    CellInfo[,] gameCells = new CellInfo[blockCount, blockSize];
    // Use this for initialization
    //    public GameObject gameOverHolder;

    public delegate void GameOver();

    public event GameOver OnGameOver;

    private static MapGen instanse;

    public static MapGen GetInstanse()
    {
        
        return instanse;
    }

    public List<CellInfo> GetUsedCells()
    {

        List<CellInfo> toReturn = new List<CellInfo>();
        for (int i = 0; i < blockCount; i++)
        {
            for (int k = 0; k < blockSize; k++)
            {
                if (gameCells[i, k].used)
                {
                    toReturn.Add(gameCells[i, k]);
                }
            }
        }
        return toReturn;
    }

    public void GO()
    {
        BlockGen.GetInstanse().DisableColliders();
        BreakField();
        Debug.Log("OnGameOver");
        if (OnGameOver != null)
            OnGameOver();
    }

    void OnDisable()
    {
        instanse = null;
        PauseMgr.GetInstanse().OnHome -= OnWentToMain;
    }

    void OnWentToMain()
    {
        
        OnGameOver = null;
        Destroy(gameObject);
    }

    void Awake()
    {
        if (instanse == null)
            instanse = this;
        

    }

    void Start()
    {
        PauseMgr.GetInstanse().OnHome += OnWentToMain;
        startY += transform.position.y;
//        gameOverHolder = GameObject.Find("GameOverHolder");
        CreateMap();
       
        PrepareSave();
    }

    void SetScore(SaveForma loaded)
    {
        SaveForma loadedGame = loaded;
//        Debug.Log(loadedGame);
        PowerUpInfo.playerCurrentScore = loadedGame.score;
        ScoreCtrl.GetInstanse().UpdateScore();
       
    }

    void PreparePowers(SaveForma loaded)
    {
        PowerUpInfo.activated = new List<PowerUps>();
        foreach (int i in loaded.savedPowerUps)
        {
            PowerUpInfo.activated.Add((PowerUps)i);
        }
        PowerUpActivator.GetInstanse().CheckPowerUps();
    }

    void PrepareField(SaveForma loaded)
    {
        List<CellSaveData> usedCells = loaded.usedCells;
        foreach (CellSaveData c in usedCells)
        {
            gameCells[c.cellBlock, c.cellPos].SetColor(Color.white, c.cellSpriteType, true);
            gameCells[c.cellBlock, c.cellPos].used = true;
        }
    }

    void PrepareBlocks(SaveForma loaded)
    {
        BlockGen.GetInstanse().PrepareSave(loaded.currentBlocks);
    }

    void PrepareSave()
    {
        if (!string.IsNullOrEmpty(PlayerPrefs.GetString("GameState", "")))
        {
//            Debug.Log(HomeCtrl.GetInstanse().savedData == null);
            SaveForma loadedGame = HomeCtrl.GetInstanse().savedData;

            SetScore(loadedGame);
            PrepareField(loadedGame);
//            PreparePowers(loadedGame);
            PrepareBlocks(loadedGame);
//            PlayerPrefs.SetInt("Save", 1);
        }
    }

    public float GetMapDistance()
    {
        if (gameCells[0, blockSize - 1] != null)
        {
            GameObject lastblockCell = gameCells[0, blockSize - 1].gameObject;
            return Vector3.Distance(transform.position, lastblockCell.transform.position) + lastblockCell.transform.lossyScale.y - 0.3f;
        }
        else
            return 0f;
    }

    public void BreakField()
    {
        for (int i = 0; i < blockCount; i++)
        {
            for (int k = 0; k < blockSize; k++)
            {
                if (gameCells[i, k].used)
                {
                    gameCells[i, k].gameObject.GetComponent<Rigidbody>().drag = Random.Range(0f, 10f);
                    
                    gameCells[i, k].gameObject.GetComponent<Rigidbody>().isKinematic = false;
                }
                else
                {
                    gameCells[i, k].StartCollapse();
                }
            }
        }
    }

    void CreateMap()
    {
        for (int i = 0; i < blockCount; i++)
        {
            for (int k = 0; k < blockSize; k++)
            {
                GameObject cell = Instantiate(cellPrefabs[k]);
                float height = startY + cellSpaceY * k;
                cell.transform.position = new Vector3(0f, height);
                cell.name = "blockId " + i + " cellId " + k;
                cell.transform.SetParent(transform);


                gameCells[i, k] = cell.GetComponent<CellInfo>();
                gameCells[i, k].blockId = i;
                gameCells[i, k].cellId = k;
                gameCells[i, k].InitColor();
            }
            transform.Rotate(0f, 0f, rotationStep);
        }
    }

    //Checks every corresponding cell for block and recolours it
    public bool CheckBlock(Indexes[] blockIndexes, int cellSpriteType, int blockId, int cellId, Color color = default(Color))
    {
        bool available = true;
        if (object.Equals(color, default(Color)))
            color = Color.black;
        RestoreCellColors();
        currentHighlight = new List<CellInfo>();
        Indexes[] blockIndexesConv = PrepareByCell(blockIndexes);
        for (int i = 0; i < blockIndexesConv.Length; i++)
        {
            Indexes cellIndex = FindCell(blockIndexesConv[i], blockId, cellId);
            if (cellIndex.byPos > blockSize - 1 || cellIndex.byPos < 0)
            {
                available = false;
            }
            else
            {
                
//                Debug.Log("gameCells " + gameCells.GetLength(0));
//                Debug.Log("gameCells " + gameCells.GetLength(1));
                currentHighlight.Add(gameCells[cellIndex.byBlock, cellIndex.byPos]);
                gameCells[cellIndex.byBlock, cellIndex.byPos].SetColor(color, cellSpriteType);
              
            }
        }
        foreach (CellInfo cell in currentHighlight)
        {
            if (cell.used)
            {
                available = !cell.used;
                break;
            }
        }
        if (!available)
        {
            foreach (CellInfo cell in currentHighlight)
            {
                cell.SetColor(color, cellSpriteType, false, true);
            }
        }

        return available;
    }
    //finds cell according to it's origin and vector
    int GetMinCell(Indexes[] BlockIndexes)
    {
        int minCellIndex = 0;
        foreach (Indexes i in BlockIndexes)
        {
            if (i.byPos < minCellIndex)
                minCellIndex = i.byPos;
        }
        return minCellIndex;
    }

    Indexes[] PrepareByCell(Indexes[] BlockIndexes)
    {
        Indexes[] cellIndexes = new Indexes[BlockIndexes.Length];
        int minCellIndex = GetMinCell(BlockIndexes);
        for (int i = 0; i < BlockIndexes.Length; i++)
        {
            cellIndexes[i] = BlockIndexes[i];
            cellIndexes[i].byPos = BlockIndexes[i].byPos - minCellIndex;
        }
        return cellIndexes;
    }

    /*
    Indexes FindCell(Indexes indexVector, int blockIdOrigin, int cellIdOrigin)
    {
        Indexes returnIdex;
        int blockIdCalc = blockIdOrigin + indexVector.byBlock;

        if (blockIdCalc < 0)
            blockIdCalc = blockCount - 1;
        else if (blockIdCalc > blockCount - 1)
            blockIdCalc = 0;
        
        returnIdex.byBlock = blockIdCalc;  
        returnIdex.byPos = cellIdOrigin + indexVector.byPos;


        return returnIdex;
    }*/

    Indexes FindCell(Indexes indexVector, int blockIdOrigin, int cellIdOrigin)
    {
        Indexes returnIdex;
        int blockIdCalc = blockIdOrigin + indexVector.byBlock;

        if (blockIdCalc < 0)
            blockIdCalc = blockCount + blockIdCalc;
        else if (blockIdCalc > blockCount - 1)
            blockIdCalc = 0 + (blockIdCalc - blockCount);

        returnIdex.byBlock = blockIdCalc;  
        returnIdex.byPos = cellIdOrigin + indexVector.byPos;


        return returnIdex;
    }

    public void CallGameCheckDelay()
    {
        StartCoroutine(CheckGODelayed());

    }

    IEnumerator CheckGODelayed()
    {
        yield return new WaitForSeconds(1f);
        CheckGameOver();
    }

    GameObject FindAvailablePos()
    {
        List<CellDescription> blocks = new List<CellDescription>(BlockGen.GetInstanse().GetCurrentBlocks());
        GameObject availableCell = null;
        for (int i = 0; i < blockCount; i++)
        {
            for (int k = 0; k < blockSize; k++)
            {
                if (gameCells[i, k] != null)
                {


                    foreach (CellDescription block in blocks)
                    {
                        if (CheckBlock(block.BlockIndexes, block.cellSpriteType, gameCells[i, k].blockId, gameCells[i, k].cellId))
                        {
                            availableCell = gameCells[i, k].gameObject;
                            break;
                        }
                    }

                }
            }
        }
        RestoreCellColors();
        return availableCell;
    }

    public void SetHint()
    {
        StartCoroutine(HighLightHint());
    }

    IEnumerator HighLightHint()
    {
        yield return new WaitForSeconds(0.05f);
        
        GameObject availableCell = FindAvailablePos();
        CellDescription aBlock;
        if (availableCell != null)
        {
            CellInfo aCell = availableCell.GetComponent<CellInfo>();
            HomeCtrl.GetInstanse().helper.transform.position = availableCell.transform.position + Vector3.down * 0.5f;
            //            HomeCtrl.GetInstanse().helper.SetActive(true);
            List<CellDescription> blocks = new List<CellDescription>(BlockGen.GetInstanse().GetCurrentBlocks());
            foreach (CellDescription block in blocks)
            {
                if (CheckBlock(block.BlockIndexes, block.cellSpriteType, aCell.blockId, aCell.cellId))
                {
                    aBlock = block;
                    break;
                }
            }
        }
        foreach (CellInfo c in currentHighlight)
        {
            c.StartHint();
        }
    }

    public void HideHint()
    {
        HomeCtrl.GetInstanse().helper.SetActive(false);
    }

    public bool CheckGameOver(CellDescription toRemove = null)
    {
        int totalpos = 0;
        bool gameOver = true;
        List<CellInfo> freeCells = new List<CellInfo>();
        List<CellDescription> blocks = new List<CellDescription>(BlockGen.GetInstanse().GetCurrentBlocks());
        if (toRemove != null)
            blocks.Remove(toRemove);
        if (blocks.Count <= 0)
            return false;
        //Debug.Log("blocksLength " + blocks.Count);
        for (int i = 0; i < blockCount; i++)
        {
            for (int k = 0; k < blockSize; k++)
            {
                if (gameCells[i, k] != null)
                {

                   
                    foreach (CellDescription block in blocks)
                    {
                        if (CheckBlock(block.BlockIndexes, block.cellSpriteType, gameCells[i, k].blockId, gameCells[i, k].cellId))
                        {
                            gameOver = false;
                            totalpos++;
                        }
                    }

                    
                }
                else
                {
                    gameOver = false;
                }
            }
        }
       
//        gameOver = true;
        if (gameOver)
        {
            bool temp = PowerUpActivator.GetInstanse().PowerUpsAvailabe();
            //Debug.Log("PUPS " + temp);
            if (temp)
                PowerUpActivator.GetInstanse().StartShake();
            gameOver = !temp;
        }
        RestoreCellColors();
//        gameOverHolder.SetActive(gameOver);
        if (gameOver)
        {
//            Debug.Log("GO");
            //*/
            PowerUpInfo.gameOver = true;
            BreakField();
            //Debug.Log("OnGameOver");
            BlockGen.GetInstanse().DisableColliders();
            if (OnGameOver != null)
                OnGameOver();
            //*/
        }
//        if (totalpos < 100 && totalpos > 0)
        if (totalpos == 1)
        {
            SetHint();
        }
        if (!gameOver)
        {
            if ((PlayerPrefs.GetInt("Tutorial", 0) != 0 && !PauseMgr.GetInstanse().tutorialFinished))
                HomeCtrl.GetInstanse().CallCreateSave();
        }
        return gameOver;
    }


    public void RestoreCellColors()
    {
        if (currentHighlight != null)
        {
            foreach (CellInfo cell in currentHighlight)
            {
                cell.RestoreColor();
            }
        }
    }


    //sets block by given values
    public bool SetBlock(Indexes[] blockIndexes, int cellSpriteType, int blockId, int cellId, Color color = default(Color))
    {
        bool available;
        if (object.Equals(color, default(Color)))
            color = Color.black;
        if (CheckBlock(blockIndexes, cellSpriteType, blockId, cellId, color))
        {
            
            //set block
            for (int i = 0; i < currentHighlight.Count; i++)
            {
                currentHighlight[i].SetColor(color, cellSpriteType, true);
                currentHighlight[i].used = true;
            }
            CheckLines(blockId, cellId);
            available = true;
            if (UndoPowerUp.GetInstanse() != null)
            {
                UndoPowerUp.GetInstanse().setCells = new List<CellInfo>(currentHighlight);

                UndoPowerUp.GetInstanse().undoAvailable = true;
                
            }
            setCells = new List<CellInfo>(currentHighlight);
            HomeCtrl.GetInstanse().AddScore(currentHighlight.Count);
            ScoreCtrl.GetInstanse().UpdateScore();
            AudioMgr.GetInstanse().PlaySound(0);
            CallGameCheckDelay();
        }
        else
        {
            foreach (CellInfo cell in currentHighlight)
            {
                cell.RestoreColor();
            }
            available = false;
        }
        return available;
    }

    public void CheckLines(int bId = -1, int cId = -1)
    {
        List<int> blocksId = FindFullBlocks();
        List<int> linesId = FindFullLines();
        int tempScore = 0;
        int tempMoney = 0;

        //tempMoney += (int)Mathf.Pow((float)blocksId.Count + (float)linesId.Count, 2f);
        tempMoney += (blocksId.Count + linesId.Count);
        if (HomeCtrl.GetInstanse().DoubleCoinsForOneGame)
        {
            tempMoney *= 2;
        }
       
        foreach (int id in blocksId)
        {
            for (int i = 0; i < blockSize; i++)
            {
                if (gameCells[id, i].used)
                    tempScore++;
                gameCells[id, i].CellResetter();
            }
        }
        tempScore *= blocksId.Count;
        HomeCtrl.GetInstanse().AddScore(tempScore);
        tempScore = 0;
        foreach (int id in linesId)
        {
            for (int i = 0; i < blockCount; i++)
            {
                if (gameCells[i, id].used)
                    tempScore++;
                gameCells[i, id].CellResetter();
            }
        }
        ClearLinesAnim(blocksId, linesId, bId, cId);
        tempScore *= linesId.Count;

        //if (!PauseMgr.GetInstanse ().tutorialFinished) {
        PowerUpInfo.AddMoney(tempMoney);
        //}
		
        ScoreCtrl.GetInstanse().UpdateScore();
        

        //HomeCtrl.GetInstanse().UpdateMoney(PowerUpInfo.playerStars);
        //moved to below:

        if (tempMoney != 0)
        {
            StartCoroutine(HomeCtrl.GetInstanse().UpdateCrystalsAfterDelay(tempMoney));
        }

    }

    void ClearLinesAnim(List<int> blocksId, List<int> linesId, int bId = -1, int cId = -1)
    {
        StartCoroutine(IClearLinesAnim(blocksId, linesId, bId, cId));
    }

    IEnumerator IClearLinesAnim(List<int> blocksId, List<int> linesId, int bId = -1, int cId = -1)
    {
        int cellId = cId >= 0 ? cId : 0;
        int blockId = bId >= 0 ? bId : 0;

        foreach (int id in blocksId)
        {
            for (int i = 0; i < blockSize; i++)
            {
                gameCells[id, i].StartClearSprite();
                yield return new WaitForSeconds(0.05f);
            }
        }
        foreach (int id in linesId)
        {
            for (int i = 0; i <= blockCount / 2; i++)
            {
                int rightI = blockId + i <= blockCount - 1 ? blockId + i : Mathf.Abs(blockCount - (blockId + i));
                int leftI = blockId - i < 0 ? blockCount - Mathf.Abs(blockId - i) : blockId - i;
//                Debug.Log("i " + i + " blockId " + blockId + " leftI " + leftI + " rightI " + rightI);

                gameCells[leftI, id].StartClearSprite();
                gameCells[rightI, id].StartClearSprite();
                yield return new WaitForSeconds(0.05f);
            }
        }
        yield return new WaitForEndOfFrame();
    }

    List<int> FindFullBlocks()
    {
        List<int> blocksId = new List<int>();
        for (int i = 0; i < blockCount; i++)
        {
            bool blockStrike = true;
            for (int k = 0; k < blockSize; k++)
            {
                if (!gameCells[i, k].used)
                {
                    blockStrike = false;
                    break;
                }
            }
            if (blockStrike)
                blocksId.Add(i);
        }

        return blocksId;
    }

    List<int> FindFullLines()
    {
        List<int> lineId = new List<int>();
        for (int i = 0; i < blockSize; i++)
        {
            bool lineStrike = true;
            for (int k = 0; k < blockCount; k++)
            {
                if (!gameCells[k, i].used)
                {
                    lineStrike = false;
                    break;
                }
            }
            if (lineStrike)
                lineId.Add(i);
        }
        return lineId;
    }

    public void BombCells(List<CellInfo> toRestore = null)
    {
        if (toRestore != null)
            currentHighlight = new List<CellInfo>(toRestore);
        foreach (CellInfo c in currentHighlight)
        {
            
            c.CellResetter();
        }
    }

    public void BombHighlight(CellInfo cell, int cellType, Color color)
    {
        RestoreCellColors();
        currentHighlight = new List<CellInfo>();            
        HighlighBlock(cell, cellType, color);
        HighlightLine(cell, cellType, color);
    }



    public void HighlighBlock(CellInfo cell, int cellType, Color color)
    {
        for (int i = 0; i < blockSize; i++)
        {
            if (!currentHighlight.Contains(gameCells[cell.blockId, i]))
                currentHighlight.Add(gameCells[cell.blockId, i]);
            gameCells[cell.blockId, i].SetColor(color, cellType);
        }
    }

    public void HighlightLine(CellInfo cell, int cellType, Color color)
    {
        for (int i = 0; i < blockCount; i++)
        {
            if (!currentHighlight.Contains(gameCells[i, cell.cellId]))
                currentHighlight.Add(gameCells[i, cell.cellId]);
            gameCells[i, cell.cellId].SetColor(color, cellType);
        }
    }

    public void UndoMove()
    {
        for (int i = 0; i < blockCount; i++)
        {
            for (int k = 0; k < blockSize; k++)
            {
                gameCells[i, k].gameObject.GetComponent<Rigidbody>().isKinematic = true;
                gameCells[i, k].RestoreCell();
            }
        }
//        Debug.Log("setCells.Count " + setCells.Count);
        if (setCells.Count > 0)
        {
            BombCells(setCells);
//            BlockGen.GetInstanse().UndoBlock(currentBlockId, blockPos);
            BlockGen.GetInstanse().SwapBlocks();
            BlockGen.GetInstanse().EnableColliders();

        }


        Invoke("TwoBombs", 0.5f);
//        Invoke("TwoBombs", 1.5f);
    }

    void TwoBombs()
    {
        BombCells(ChooseUndoCells());
        BombCells(ChooseUndoCellsHammer());
    }

    List<CellInfo> ChooseUndoCellsHammer()
    {
        List<CellInfo> choosen = new List<CellInfo>();

        int randomBlock, randomCell;
        randomBlock = Random.Range(0, blockCount);
        randomCell = Random.Range(0, blockSize);
//        PowerUpActivator.GetInstanse().StartMoveHammer(transform.position, gameCells[randomBlock, randomCell].gameObject.transform.position);
        for (int i = 0; i < 3; i++)
        {
            for (int k = 0; k < 3; k++)
            {
                Indexes temp = new Indexes(i, k);
                Indexes ind = FindCell(temp, randomBlock, randomCell);
                choosen.Add(gameCells[ind.byBlock, ind.byPos]);
            }
        }
       
        return choosen;
    }

    List<CellInfo> ChooseUndoCells()
    {

        List<CellInfo> choosen = new List<CellInfo>();
        int randomBlock, randomCell;
        randomBlock = Random.Range(0, blockCount);
        randomCell = Random.Range(0, blockSize);
//        PowerUpActivator.GetInstanse().StartMoveBomb(transform.position, gameCells[randomBlock, randomCell].gameObject.transform.position);
        for (int i = 0; i < blockCount; i++)
        {
            choosen.Add(gameCells[i, randomCell]);
        }
        for (int i = 0; i < blockCount; i++)
        {
            if (i != randomCell)
                choosen.Add(gameCells[randomBlock, i]);
        }
        return choosen;
    }
}
