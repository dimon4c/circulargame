﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;



public struct Indexes
{
    public int byBlock;
    public int byPos;

    public Indexes(int byBlock, int byPos)
    {
        this.byBlock = byBlock;
        this.byPos = byPos;
    }
}


public class CellDescription : MonoBehaviour
{
    protected const int blockSize = 4;
    protected const int blockCount = 4;
    float startY = 0.3f;
    const float cellSpaceY = 0.15f;
    const float rotationStep = 45f;
    GameObject[,] gameCells = new GameObject[blockCount, blockSize];

    protected Vector3 startPos;
    protected Vector3 startLocalPos;
    public GameObject cellHolder;
    public List<Sprite> cellSprite;
    float defHeight;
    public Indexes[] blockIndexes;
    //    bool enabled = true;
    /*
    protected float horizontalStep = 5.5f;
    protected float verticalStep = 3f;
    */
    protected float scaleModifier = 0.3f;
    public int blockIndex;
    List<List<SpriteRenderer>> renderers = new List<List<SpriteRenderer>>();

    public virtual Indexes[] BlockIndexes
    {
        set
        {
            blockIndexes = new Indexes[]{ };
        }
        get
        {
            return blockIndexes;
        }
    }

    protected Color blockColor = Color.white;
    public int cellSpriteType;

    protected  CellInfo collidedCell;
    protected bool collided = false;

    public GameObject mapHolder;
    protected float maxRadius = 0.07f;


    public float topDistance;
    protected Quaternion defaultRotation;

    int GetMinBlock()
    {
        int minBlockIndex = 0;
        foreach (Indexes i in BlockIndexes)
        {
            if (i.byBlock < minBlockIndex)
                minBlockIndex = i.byBlock;
        }
        return minBlockIndex;
    }

    int GetMinCell()
    {
        int minCellIndex = 0;
        foreach (Indexes i in BlockIndexes)
        {
            if (i.byPos < minCellIndex)
                minCellIndex = i.byPos;
        }
        return minCellIndex;
    }

    protected int GetBlockWidth()
    {
        List<int> blockWidth = new List<int>();
        foreach (Indexes i in BlockIndexes)
        {
            int ind = i.byBlock;
            if (!blockWidth.Contains(ind))
                blockWidth.Add(ind);
        }
//        Debug.Log(blockWidth.Count);
        return blockWidth.Count;
    }

    protected int GetBlockHeight()
    {
        List<int> blockHeight = new List<int>();
        foreach (Indexes i in BlockIndexes)
        {
            if (!blockHeight.Contains(i.byPos))
                blockHeight.Add(i.byPos);
        }
        return blockHeight.Count;
    }

    protected void CreateMap()
    {
        List<Indexes> ind = new List<Indexes>(BlockIndexes);
//        int minBlockIndex = GetMinBlock();
//        int minCellIndex = GetMinCell();
        for (int i = 0; i < blockCount; i++)
        {
           
            for (int k = 0; k < blockSize; k++)
            {
                Indexes ci;
                ci.byBlock = i - 1;
                ci.byPos = k;
                if (ind.Contains(ci))
                {

                    GameObject cell = new GameObject("CellImage_" + k);
                    SpriteRenderer renderer = cell.AddComponent<SpriteRenderer>();

                    renderer.sprite = cellSprite[k];
                    renderer.flipY = true;
                    float x = cellHolder.transform.position.x;
                    float height = cellHolder.transform.position.y + startY + cellSpaceY * k;
                    cell.transform.position = new Vector3(x, height);
                    cell.transform.localScale = new Vector3(cell.transform.localScale.x * scaleModifier, cell.transform.localScale.y * scaleModifier);
                    cell.transform.SetParent(cellHolder.transform);



                    gameCells[i, k] = cell;
                }
            }
            cellHolder.transform.Rotate(0f, 0f, rotationStep);
        }
//        cellHolder.transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
        float angle = rotationStep * blockCount / 2 + -rotationStep / 2;
//        float angle = rotationStep * blockCount / 2;
        cellHolder.transform.Rotate(0f, 0f, angle);
        int blockWidth = GetBlockWidth();
        if (blockWidth % 2 != 0)
        {
            
//            Debug.Log("Rotating");
//            if (blockWidth == 1)
//                cellHolder.transform.Rotate(0f, 0f, rotationStep / 2);
//            if (blockWidth == 3 || blockWidth == 1)
            cellHolder.transform.Rotate(0f, 0f, -rotationStep / 2);
        }
        else if (blockWidth == 2)
        {
            bool containsZero = false;
            foreach (Indexes i in BlockIndexes)
            {
                if (i.byBlock == 0)
                {
                    containsZero = true;
                    break;
                }
            }
            if (!containsZero)
                cellHolder.transform.Rotate(0f, 0f, -rotationStep / 2);
            
        }
        //FIXME: 
        //after default rotation we need to rotate according to blocks count
        if (GetBlockHeight() > 1)
            cellHolder.transform.position = cellHolder.transform.position + new Vector3(0f, 0.15f);
//        this.gameObject.transform.GetChild(0).position = this.gameObject.transform.GetChild(0).position + new Vector3(0f, 0.18f);
        //*/
        if (GetBlockWidth() == 4)
        {
            BoxCollider b = gameObject.GetComponent<BoxCollider>();
            Vector3 prop = b.center;
            prop.y = -0.3f;
            b.center = prop;

//            Vector3 bPos = gameObject.transform.localPosition;
//            bPos.y += -0.4f;
//            bPos.x = -0.5f;
//            transform.localPosition = bPos;
        }
        else if (GetBlockHeight() == 1)
        {
            BoxCollider b = gameObject.GetComponent<BoxCollider>();
            Vector3 prop = b.center;
            prop.y = -1f;
            b.center = prop;
        }
        //*/
    }

    public void SetBlockAfterSpawn()
    {
        if (GetBlockHeight() == 1)
        {
            Vector3 bPos = gameObject.transform.localPosition;
            bPos.y += -0.2f;
            //            bPos.x = -0.5f;
            transform.localPosition = bPos;
        }
    }
    /*/
    protected void CreateImage()
    {
        for (int i = 0; i < BlockIndexes.Length; i++)
        {
            int minBlockIndex = GetMinBlock();
            int minCellIndex = GetMinCell();
            GameObject go = new GameObject("CellImage_" + i);
            SpriteRenderer renderer = go.AddComponent<SpriteRenderer>();
            renderer.sprite = cellSprite[BlockIndexes[i].byPos - minCellIndex];
            go.transform.SetParent(cellHolder.transform);
            go.transform.localScale = new Vector3(go.transform.localScale.x * scaleModifier, go.transform.localScale.y * scaleModifier);

            float horPos;
            float verPos;
            if (BlockIndexes[i].byBlock != 0 && BlockIndexes[i].byPos != 0)
                horPos = -horizontalStep * BlockIndexes[i].byBlock + (-horizontalStep * BlockIndexes[i].byPos);
            else
                horPos = -horizontalStep * BlockIndexes[i].byBlock;
            verPos = -verticalStep * BlockIndexes[i].byPos;
            go.transform.localPosition = new Vector3(horPos, verPos);
            go.transform.Rotate(0f, 0f, -rotationStep * BlockIndexes[i].byBlock);
            go.name = BlockIndexes[i].byBlock + "_" + BlockIndexes[i].byPos;
        }

    }
    //*/

    void OnDisable()
    {
        BlockGen.GetInstanse().OnRemoveBlocks -= OnRemove;


    }

    public void ChangeSprite(int type)
    {
        cellSpriteType = type;
        cellSprite = new List<Sprite>();
        for (int i = 0; i < blockSize; i++)
        {
            cellSprite.Add(HomeCtrl.GetInstanse().BBSprites[cellSpriteType, i]);
        }
        Transform[] temp = cellHolder.GetComponentsInChildren<Transform>();
        for (int i = 1; i < temp.Length; i++)
        {
            Destroy(temp[i].gameObject);
        }
        CreateMap();

        /*/
        for (int k = 0; k < renderers.Count; k++)
        {
            renderers[k].sprite = cellSprite[k];

        }
        //*/
    }

   

    public virtual void Start()
    {
        cellSpriteType = UnityEngine.Random.Range(1, HomeCtrl.spriteTypesCount);
        this.gameObject.transform.GetChild(0).GetComponent<DragControl>().SetParentScript(this);
        cellSprite = new List<Sprite>();
        for (int i = 0; i < blockSize; i++)
            cellSprite.Add(HomeCtrl.GetInstanse().BBSprites[cellSpriteType, i]);
        mapHolder = GameObject.Find("MapHolder");
        CreateMap();
        startPos = transform.position;
        defaultRotation = transform.rotation;
        startLocalPos = transform.localPosition;
        topDistance = MapGen.GetInstanse().GetMapDistance();
        BlockGen.GetInstanse().OnRemoveBlocks += OnRemove;
       
       
    }

    public virtual void OnRemove()
    {
        BlockGen.GetInstanse().OnRemoveBlocks -= OnRemove;
        BlockGen.GetInstanse().BlockOn--;

           
        if (UndoPowerUp.GetInstanse() != null)
        {
            UndoPowerUp.GetInstanse().blockLocalPosition = startLocalPos;
            UndoPowerUp.GetInstanse().currentBlockId = blockIndex;
            UndoPowerUp.GetInstanse().blockSprite = cellSpriteType;
            MapGen.GetInstanse().currentBlockId = blockIndex;
            MapGen.GetInstanse().blockPos = startLocalPos;
        }

        Destroy(gameObject);

    }

    public virtual void RotateBlock()
    {
        if (mapHolder == null)
        {
            mapHolder = GameObject.Find("MapHolder");
            return;
        }
//        bool temp = Vector3.Distance(cellHolder.transform.position, mapHolder.transform.position) <= maxRadius;
//        Debug.Log("maxRadius " + temp);
        /*/
        Vector3 point = mapHolder.transform.position;
        point.y = 0.0f;
        cellHolder.transform.LookAt(point, cellHolder.transform.position);

//        cellHolder.transform.LookAt(mapHolder.GetComponent<Transform>());
        //*/
        if (Vector3.Distance(cellHolder.transform.position, mapHolder.transform.position) <= maxRadius || Vector3.Distance(cellHolder.transform.position, mapHolder.transform.position) > topDistance)
        {
            collidedCell = null;
            cellHolder.SetActive(true);
            MapGen.GetInstanse().RestoreCellColors();
        }
        if (MapGen.GetInstanse().currentHighlight.Count < 0)
        {
            collidedCell = null;
            cellHolder.SetActive(true);
        }
        Vector3 targetDir = mapHolder.transform.position - transform.position;

       
        float angle = Mathf.Atan2(targetDir.x, targetDir.y) * Mathf.Rad2Deg;

        Vector3 targetDir2 = mapHolder.transform.position - transform.position;
       

        transform.rotation = Quaternion.Euler(0.0f, 0.0f, -angle);


        //*/
    }


  

    public void OnMouseUp2(Vector3 startPos)
    {
        if (collidedCell == null || !MapGen.GetInstanse().SetBlock(BlockIndexes, cellSpriteType, collidedCell.blockId, collidedCell.cellId, blockColor))
        {
            transform.position = this.startPos;
            transform.rotation = defaultRotation;
            cellHolder.SetActive(true);
        }
        else
        {
            /*/
            if (MapGen.GetInstanse() != null && !PowerUpInfo.onPause)
                MapGen.GetInstanse().CheckGameOver(this);
            //*/
            OnRemove();
        }
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
//        Debug.Log(other.gameObject.name);
        /*/
        if (collidedCell == null)
        {
            collidedCell = other.gameObject.GetComponent<CellInfo>();
            MapGen.GetInstanse().CheckBlock(BlockIndexes, collidedCell.blockId, collidedCell.cellId, blockColor);
        }

        if (other.gameObject.GetComponent<CellInfo>() != collidedCell)
        {
            if (Vector3.Distance(other.gameObject.transform.position, transform.position) < Vector3.Distance(collidedCell.gameObject.transform.position, transform.position))
            {
                collidedCell = other.gameObject.GetComponent<CellInfo>();
//                MapGen.GetInstanse().RestoreCellColors();
                MapGen.GetInstanse().CheckBlock(BlockIndexes, collidedCell.blockId, collidedCell.cellId, blockColor);
            }
        }
        //*/
        //*/
        cellHolder.SetActive(false);
//            collided = true;
        collidedCell = other.gameObject.GetComponent<CellInfo>();

        MapGen.GetInstanse().CheckBlock(BlockIndexes, cellSpriteType, collidedCell.blockId, collidedCell.cellId, blockColor);

        //*/
    }


    //*/
    protected virtual   void OnTriggerExit(Collider other)
    {
        /*/
        if (other.gameObject.GetComponent<CellInfo>() == collidedCell)
        {
            collided = false;
            collidedCell = null;
            MapGen.GetInstanse().RestoreCellColors();
        }
        // Restore status
        //*/
    }
    //*/
   
}
