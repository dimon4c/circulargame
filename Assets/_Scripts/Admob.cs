﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class Admob : MonoBehaviour
{

    public static Admob Instance;

    string BannerId = "ca-app-pub-5909288115715017/3832836681";
    string InterstitialId = "ca-app-pub-5909288115715017/2356103487";
    string StaticInterstitialId = "ca-app-pub-5909288115715017/6786303084";
    string VideoId = "ca-app-pub-5909288115715017/8263036289";


    BannerView bannerView;
    InterstitialAd interstitial;
    InterstitialAd staticInterstitial;
    RewardBasedVideoAd rewardBasedVideo;


    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {

        LoadBanner();
        LoadInterstitial();
        LoadStaticInterstitial();

        rewardBasedVideo = RewardBasedVideoAd.Instance;
        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;

        LoadVideo();

    }

    //---------------------------------------------------------------------------
    void LoadBanner()
    {
        if (HasAds())
        {
            bannerView = new BannerView(BannerId, AdSize.Banner, AdPosition.Top);
            AdRequest request = new AdRequest.Builder().Build();
            bannerView.LoadAd(request);
        }
        //Debug.Log("Banner Loaded");
    }
    //---------------------------------------------------------------------------
    void LoadInterstitial()
    {
        if (HasAds())
        {
            interstitial = new InterstitialAd(InterstitialId);
            AdRequest request = new AdRequest.Builder().Build();
            interstitial.LoadAd(request);
        }
    }
    //---------------------------------------------------------------------------
    void LoadStaticInterstitial()
    {
        if (HasAds())
        {
            staticInterstitial = new InterstitialAd(StaticInterstitialId);
            AdRequest request = new AdRequest.Builder().Build();
            staticInterstitial.LoadAd(request);
        }
    }
    //---------------------------------------------------------------------------
    public void LoadVideo()
    {        
        AdRequest request = new AdRequest.Builder().Build();
        rewardBasedVideo.LoadAd(request, VideoId);
    }
    //---------------------------------------------------------------------------
    public void ShowInterstitial()
    {
        if (!HasAds())
            return;

        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }
        LoadInterstitial();
    }
    //---------------------------------------------------------------------------
    public void ShowStaticInterstitial()
    {
        if (!HasAds())
            return;

        if (staticInterstitial.IsLoaded())
        {
            staticInterstitial.Show();
        }
        LoadStaticInterstitial();
    }
    //---------------------------------------------------------------------------
    public void RemoveBanner()
    {
        if (bannerView != null)
        {
            bannerView.Hide();
            bannerView.Destroy();
        }
    }
    //---------------------------------------------------------------------------
    int VideoPurpose = 0;

    public void ShowRewardBasedVideo(int purpose) //1 for coins, 2 for revive, 3 for 2x coins
    {
        if (RewardBasedVideoAd.Instance.IsLoaded())
        {
            if (purpose == 1)
            {
                HomeCtrl.GetInstanse().powerupsWatchAdBtn.SetActive(false);
            }
            else if (purpose == 2)
            {
                //HomeCtrl.GetInstanse().videobtnRevive.SetActive(false);
            }
            else if (purpose == 3)
            {
                HomeCtrl.GetInstanse().videobtnx2.SetActive(false);
            }
            VideoPurpose = purpose;
            RewardBasedVideoAd.Instance.Show();
        }
        else
        {
            LoadVideo();
            //Debug.Log("No video currently available. Please try again later.");
        }

    }
    //---------------------------------------------------------------------------
    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;

        if (VideoPurpose == 1)
        {            
            HomeCtrl.GetInstanse().OnRewardedVideo1Watched();
        }
        else if (VideoPurpose == 2)
        {
            HomeCtrl.GetInstanse().OnRewardedVideo2Watched();
        }
        else if (VideoPurpose == 3)
        {
            HomeCtrl.GetInstanse().OnRewardedVideo3Watched();
        }

        VideoPurpose = 0;

        if (!RewardBasedVideoAd.Instance.IsLoaded())
        {
            LoadVideo();
        }
    }
    //---------------------------------------------------------------------------
    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        if (RewardBasedVideoAd.Instance.IsLoaded())
        {
          
//                HomeCtrl.GetInstanse().powerupsWatchAdBtn.SetActive(true);
         
            //HomeCtrl.GetInstanse().videobtnRevive.SetActive(false);
            if (HasDoubleCoins() || HomeCtrl.GetInstanse().DoubleCoinsForOneGame)
            {
                HomeCtrl.GetInstanse().videobtnx2.SetActive(false);
            }
            else
            {
				if (!(PlayerPrefs.GetInt ("Tutorial", 0) == 0 || PauseMgr.GetInstanse ().tutorialFinished)) {
					HomeCtrl.GetInstanse ().videobtnx2.SetActive (true);
				}
            }

        }
    }
    //---------------------------------------------------------------------------
    public bool HasAds()
    {
        return PlayerPrefs.GetInt("noads", 0) == 0;
    }
    //---------------------------------------------------------------------------
    public bool HasDoubleCoins()
    {
        return PlayerPrefs.GetInt("2xcoins", 0) == 1;
    }
    //---------------------------------------------------------------------------
    public bool DoubleCoinsForOneGame()
    {
        return HomeCtrl.GetInstanse().DoubleCoinsForOneGame;
    }
    //---------------------------------------------------------------------------
    public bool IsRewardedVideoLoaded()
    {
        return RewardBasedVideoAd.Instance.IsLoaded();
    }
    //---------------------------------------------------------------------------
}
