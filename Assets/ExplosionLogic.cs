﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionLogic : MonoBehaviour
{
    void StartAnim()
    {
        gameObject.transform.parent.GetChild(0).gameObject.SetActive(false);
    }
    // Use this for initialization
    public void EndAnim()
    {
        gameObject.transform.parent.GetChild(0).gameObject.SetActive(true);
        gameObject.SetActive(false);
        gameObject.transform.parent.gameObject.SetActive(false);
    }
}